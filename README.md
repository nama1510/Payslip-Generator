# Payslip Generator

This program reads a CSV file and does the following:
  * Checks if the Task Identification Number (NIF/NIE) of each employee is correct
    * If it is not correct it calculates one and writes it to the csv file. It creates an XML files with the errors
  * Creates an email for each employee
  * Generates an IBAN number
  
  * Calculates a Pay-slip for each employee for a given month and year taking on account different variables, then it
  exports it to a Database and generates PDF files for each payslip.
  
 # What I learned in this project
 
  * Reading and writing CSV files in Java.
  * Read and write XML files.
  * Connect a Java application with a database
  * Creation and maintenance of databases with hibernate
  * Create PDF files in java
  * More object oriented programming
  * Abstraction
