package org.application;

import org.data.BrutoRetenciones;
import org.data.CuotaEmpresa;
import org.data.Nomina;
import org.data.Worker;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Locale;

public class CalculadorNominas {
    //private static ArrayList<Category> categorias = new ArrayList<Category>();
    //private static double[][] cotizacionSeguridadSocial = new double[11][2];

    //El ArrayList de trabajadores contiene todos los trabajadores del excell.
    //Cada trabajador contiene sus datos y la categoria a la que pertenece la cual es una clase en si misma
    //para acceder a los valores de la categoria simplement basta con llamar a los metodos get de la categoria
    private ArrayList<Worker> allWorkers = new ArrayList<Worker>();

    //Contiene Objetos de la clase BrutoRetenciones en las cuales se puede acceder a los metodos get para obtener sus diferentes valores.
    private static ArrayList<BrutoRetenciones> brutoRetenciones =  new ArrayList<BrutoRetenciones>();

    //En el array valoresTrienio se accede al importe bruto segun el tienio.
    //El indice del array es el trienio y su valor es el importe bruto.
    //por ejemplo si queremos acceder al valor de decimo trienio (10) basta con escribir "valoresTrienio[10]."
    private static double[] valoresTrienio = new double[19];

    //Contiene Objetos del tipo CuotaEmpresa. Se puede acceder al nombre de cada cuota y a su valor.
    private static ArrayList<CuotaEmpresa> cuotasEmpresa = new ArrayList<CuotaEmpresa>();
    private ArrayList<Nomina> nominasGeneradas = new ArrayList<Nomina>();

    private String mesString;
    private int mes;
    private String anioString;
    private int anio;

    //Prorrateos
    private double brutoProrrateo;
    private double brutoNoProrrateo;
    private double prorrateo;

    //Deducciones Trabajador
    private double ss;
    private double desempleo;
    private double formacion;
    private double irpf;
    private double liquidoNomina;


    //Costes Empresario
    private double contingenciasComunes;
    private double fogasa;
    private double desempleo_Emp;
    private double formacion_Emp;
    private double accidentes;
    private double costeTotalTrabajador;
    private Double totalEmpresario;

    //Fechas
    LocalDate input_date;
    LocalDate fechaAltaLabora_date;
    LocalDate fechaBaja_date;
    LocalDate fechaAlta_date;

    Long mesesAntiguedad;
    Long aniosAntiguedad;
    double brutoTrabajador;
    double costeBaseEmpresario;
    boolean isAnioCambioTrienio = false;


    public CalculadorNominas(ArrayList<Worker> allWorkers,double[] valoresTrienio, ArrayList<CuotaEmpresa> cuotasEmpresa, ArrayList<BrutoRetenciones> brutoRetenciones, int mes, int anio){
        this.allWorkers = allWorkers;
        this.valoresTrienio = valoresTrienio;
        this.cuotasEmpresa = cuotasEmpresa;
        this.brutoRetenciones = brutoRetenciones;

        this.mes = mes;
        this.anio = anio;
        this.mesString = monthToName(mes);
        this.anioString = String.valueOf(anio);



        calcularNomina();

    }
    //TODO: Tener en cuenta los anios bisiestos.
    //TODO: El PDF se generara desde esta clase, no el main.
    public void calcularNominaExtra(Worker w){

        brutoTrabajador = calcularBrutoTrabajador(w);
        costeBaseEmpresario = costeBaseEmpresario(w);
        calcularDeduccionesTrabajador(w, true);
        calcularTrienios(w);


        calcularTrienios(w);//Cambiar Esto
        if(calcularTrienios(w) == 0){
            brutoNoProrrateo = prorrateo * mesesAntiguedad;
        }

        PDFReader nominaPDF = new PDFReader(w,
                String.valueOf(liquidoNomina),
                String.valueOf(prorrateo),
                String.valueOf(w.getCategoria().getComplementos()),
                String.valueOf(calcularTrienios(w)),
                String.valueOf(valoresTrienio[calcularTrienios(w)]),
                String.valueOf(brutoTrabajador),
                String.valueOf(ss),
                String.valueOf(desempleo),
                String.valueOf(formacion),
                String.valueOf(irpf),
                String.valueOf(retencion),
                mesString,
                anioString,
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                true,
                cuotasEmpresa,
                String.valueOf(brutoAnio));

        nominaPDF.getNomina().setTrabajador(w);
        nominaPDF.getNomina().setAnio(this.anio);
        nominaPDF.getNomina().setMes(this.mes);
        if(nominaPDF.getNomina().getTrabajador().getId() != "")
            nominasGeneradas.add(nominaPDF.getNomina());

    }
    public void calcularNomina(){
        //Realiza las diferentes operaciones para calcular nominas.
        for (int i = 0 ; i < allWorkers.size(); i++) {
            brutoTrabajador = calcularBrutoTrabajador(allWorkers.get(i));
            costeBaseEmpresario = costeBaseEmpresario(allWorkers.get(i));
            calcularTrienios(allWorkers.get(i));
            calcularDeduccionesTrabajador(allWorkers.get(i), false);
            calcularCosteEmpresario(allWorkers.get(i));

            PDFReader nominaPDF = new PDFReader(allWorkers.get(i),
                    String.valueOf(liquidoNomina),
                    String.valueOf(prorrateo),
                    String.valueOf(allWorkers.get(i).getCategoria().getComplementos()),
                    String.valueOf(calcularTrienios(allWorkers.get(i))),
                    String.valueOf(valoresTrienio[calcularTrienios(allWorkers.get(i))]),
                    String.valueOf(brutoTrabajador),
                    String.valueOf(ss),
                    String.valueOf(desempleo),
                    String.valueOf(formacion),
                    String.valueOf(irpf),
                    String.valueOf(retencion),
                    mesString,
                    anioString,
                    String.valueOf(contingenciasComunes),
                    String.valueOf(fogasa),
                    String.valueOf(desempleo_Emp),
                    String.valueOf(formacion_Emp),
                    String.valueOf(accidentes),
                    String.valueOf(costeTotalTrabajador),
                    String.valueOf(totalEmpresario),
                    false,
                    cuotasEmpresa,
                    String.valueOf(brutoAnio));

            nominaPDF.getNomina().setTrabajador(allWorkers.get(i));
            nominaPDF.getNomina().setAnio(this.anio);
            nominaPDF.getNomina().setMes(this.mes);
            if(nominaPDF.getNomina().getTrabajador().getId() != "")
                nominasGeneradas.add(nominaPDF.getNomina());

            if(!allWorkers.get(i).isProrrataExtra()){
                if(mesString.equals("Junio") || mesString.equals("Diciembre")) {
                    calcularNominaExtra(allWorkers.get(i));
                }
            }
        }
    }
    double brutoAnio;
    private double calcularBrutoTrabajador(Worker w){
        //TODO: Calcular aqui trienio
        double res;
         brutoProrrateo = valoresTrienio[calcularTrienios(w)] + (w.getCategoria().getSalarioBase() + w.getCategoria().getComplementos()) / 12; //TODO: Sumar prorrateo.
         brutoNoProrrateo = valoresTrienio[calcularTrienios(w)] + (w.getCategoria().getSalarioBase() + w.getCategoria().getComplementos()) / 14;
        if(!w.isProrrataExtra()) {
            res = brutoNoProrrateo;
            //brutoAnio = 14*valoresTrienio[calcularTrienios(w)] + (w.getCategoria().getSalarioBase() + w.getCategoria().getComplementos());
            prorrateo = 0;
        }else {
            res = brutoProrrateo;
            //brutoAnio = 12*valoresTrienio[calcularTrienios(w)] + (w.getCategoria().getSalarioBase() + w.getCategoria().getComplementos());
            prorrateo = brutoProrrateo - brutoNoProrrateo;
        }


        int trienio = calcularTrienios(w);
        double valorTrienio = 0;
        if(mesesAntiguedad - trienio * 36 > 12){
            valorTrienio = valoresTrienio[trienio] * 12;
        }else{

            valorTrienio =(valoresTrienio[trienio] * (mesesAntiguedad - (trienio * 36))) + valoresTrienio[trienio-1] * (12 -  (mesesAntiguedad - (trienio * 36)));
        }
        //FIXME: El bruto a veces da negativo
        if(valorTrienio > 0)
            brutoAnio = valorTrienio + (w.getCategoria().getSalarioBase() + w.getCategoria().getComplementos());
        else
            brutoAnio = 12500;

        brutoAnio = valorTrienio + (w.getCategoria().getSalarioBase() + w.getCategoria().getComplementos());
       // System.out.println(valorTrienio);

        return res;
    }
    public double costeBaseEmpresario(Worker w){
        return brutoProrrateo;
    }


    double retencion = -1;
    public void calcularDeduccionesTrabajador(Worker w,boolean isExtra){
        double brutoMes = calcularBrutoTrabajador(w);

//        if(!w.isProrrataExtra()){
//            brutoAnio = brutoMes * 12;
//        }else{
//            brutoAnio = brutoMes * 14;
//        }

//        System.out.println(cuotasEmpresa.get(0).getNombreCuota()+"\n" +
//                cuotasEmpresa.get(1).getNombreCuota() + "\n" +
//                cuotasEmpresa.get(2).getNombreCuota());
        if(!isExtra) {
            ss = brutoMes * cuotasEmpresa.get(0).getValor() / 100;
            desempleo = brutoMes * cuotasEmpresa.get(1).getValor() / 100;
            formacion = brutoMes * cuotasEmpresa.get(2).getValor() / 100;
        }else{
            ss = 0;
            desempleo = 0;
            formacion = 0;
        }
        for (int i = 1; i < brutoRetenciones.size(); i++){
            if(brutoRetenciones.get(i).getBruto() > brutoAnio){
                //System.out.println("Retenciones: " + brutoRetenciones.get(i - 1).getBruto() + " | " + brutoRetenciones.get(i - 1).getRetenciones());
                retencion = brutoRetenciones.get(i-1).getRetenciones();
                break;
            }
        }
        if(retencion >= 0)
            irpf = (brutoMes) * retencion / 100;
        else
            System.out.println("Error al calcular las retenciones en caclularDeducciones.");

        if(!isExtra)
            liquidoNomina = brutoMes - (ss + desempleo + formacion + irpf) + valoresTrienio[calcularTrienios(w)] +w.getCategoria().getComplementos()/12;
        else {

            int trienio = calcularTrienios(w);
            double valorTrienio;
            if(mesesAntiguedad - trienio * 36 > 6){
                valorTrienio = valoresTrienio[trienio] * 6;
            }else{
                valorTrienio =(valoresTrienio[trienio] * (mesesAntiguedad - (trienio * 36))) + valoresTrienio[trienio-1] * (6 -  (mesesAntiguedad - (trienio * 36)));
            }

            liquidoNomina = brutoMes - (irpf) + valorTrienio + w.getCategoria().getComplementos() / 12;
        }
    }

    public void calcularCosteEmpresario(Worker w){
        double cbe = costeBaseEmpresario(w);

        contingenciasComunes =  cbe * cuotasEmpresa.get(3).getValor()/100;
        fogasa =                cbe * cuotasEmpresa.get(4).getValor()/100;
        desempleo_Emp =         cbe * cuotasEmpresa.get(5).getValor()/100;
        formacion_Emp =         cbe * cuotasEmpresa.get(6).getValor()/100;
        accidentes =            cbe * cuotasEmpresa.get(7).getValor()/100;

        totalEmpresario = contingenciasComunes + fogasa + desempleo_Emp+formacion_Emp+accidentes;
        costeTotalTrabajador = cbe + contingenciasComunes + fogasa + desempleo_Emp+formacion_Emp+accidentes;
    }
    private int calcularTrienios(Worker w) {
        //TODO: Calcular diferencia de fechas.
        CalcularFechas(w);
        mesesAntiguedad = ChronoUnit.MONTHS.between(fechaAltaLabora_date,input_date);
        aniosAntiguedad = ChronoUnit.YEARS.between(fechaAltaLabora_date,input_date);

        if(aniosAntiguedad % 3 == 0)
            isAnioCambioTrienio = true;
        else
            isAnioCambioTrienio = false;
        int trienios = (int) Math.floor(aniosAntiguedad/3);
        long trienioMod = mesesAntiguedad%36;
        //int trienios = (int) Math.floor(mesesAntiguedad/36);
        if(trienios < 0) trienios = 0;
        if(trienios > 18) trienios = 18;

        if(trienioMod == 0 && trienios != 0){
            trienios -= 1;
        }
        return trienios;
    }
    int diasBaja;
    private int calcularDiasBaja(Worker w){


        return 1;
    }

    private void CalcularFechas(Worker w){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        formatter = formatter.withLocale(Locale.ENGLISH);
        String fechaInput;
        if(mes < 10)
            fechaInput = "01/0" + String.valueOf(mes) + "/" + String.valueOf(anio);
        else
            fechaInput = "01/" + String.valueOf(mes) + "/" + String.valueOf(anio);
        input_date = LocalDate.parse(fechaInput,formatter);
        fechaAltaLabora_date = LocalDate.parse(w.getFechaAltaEmpresa(), formatter);

        if(!w.getFechaBajaLaboral().equals(""))
            fechaBaja_date = LocalDate.parse(w.getFechaBajaLaboral(),formatter);
        if(!w.getFechaAltaLaboral().equals(""))
            fechaAlta_date = LocalDate.parse(w.getFechaAltaLaboral(), formatter);

    }
    private String monthToName(int mes){
        String ret = "";
        switch (mes){

            case 1:
                ret = "Enero";
                break;
            case 2:
                ret = "Febrero";
                break;
            case 3:
                ret = "Marzo";
                break;
            case 4:
                ret = "Abril";
                break;
            case 5:
                ret = "Mayo";
                break;
            case 6:
                ret = "Junio";
                break;
            case 7:
                ret = "Julio";
                break;
            case 8:
                ret = "Agosto";
                break;
            case 9:
                ret = "Septiembre";
                break;
            case 10:
                ret = "Octubre";
                break;
            case 11:
                ret = "Noviembre";
                break;
            case 12:
                ret = "Diciembre";
                break;
            default:
                ret = "";
                break;
        }
        return ret;
    }

    public ArrayList<Nomina> getNominasGeneradas() {
        return nominasGeneradas;
    }
}
