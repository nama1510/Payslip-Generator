package org.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.controller.Operaciones;
import org.data.*;

enum Type {NIF, NIE};


/***
 * Excell Reader maneja la lectura y escritura del archivo excel base.
 */
public class ExcelReader {

	private ArrayList<String> NIFsNIEs =  new ArrayList<String>();
	private  ArrayList<String> NIFsNIEsRepetidos = new ArrayList<String>();
	private ArrayList<Worker> repeatedWorkers = new ArrayList<Worker>();
	private ArrayList<Worker> allWorkers = new ArrayList<Worker>();
 	private static ArrayList<Category> categorias = new ArrayList<Category>();
	private static ArrayList<BrutoRetenciones> brutoRetenciones =  new ArrayList<BrutoRetenciones>();
	private static double[][] cotizacionSeguridadSocial = new double[11][2];
	private static double[] valoresTrienio = new double[19];
	private static ArrayList<CuotaEmpresa> cuotasEmpresa = new ArrayList<CuotaEmpresa>();


	private static String nifNieCellString = "NIF/NIE";
	private static String lastName1CellString = "Apellido1";
	private static String lastName2CellString = "Apellido2";
	private static String nameCellString = "Nombre";
	private static String emailCellString = "Email";
	private static String categoriaCellString = "Categoria";
	private static String companyString = "Nombre empresa";
	private static String cifCellString = "Cif empresa";
	private static String FechaAltaEmpresaCellString = "FechaAltaEmpresa";
	private static String FechaAltaLaboralCellString = "FechaAltaLaboral";
	private static String FechaBajaLaboralCellString = "FechaBajaLaboral";
	private static String horasExtraForzadasCellString = "HorasExtraForzadas";
	private static String horasExtraVoluntariasCellString = "HorasExtraVoluntarias";
	private static String prorrataExtraCellString = "ProrrataExtra";
	private static String cccCellString = "CodigoCuenta";
	private static String paisOrigenString = "Pais Origen Cuenta Bancaria";
	private static String ibanCellString = "IBAN";
	private static String numTrieniosCellString = "N�mero de trienios";
	private static String imorteBrutoCellString = "Importe bruto";

	private int nifNieCellIndex = -1;
	private int lastName1CellIndex = -1;
	private int lastName2CellIndex = -1;
	private int nameCellIndex = -1;
	private int emailCellIndex = -1;
	private int categoriaCellIndex = -1;
	private int companyCellIndex = -1;
	private int cifCellIndex = -1;
	private int FechaAltaEmpresaCellIndex = -1;
	private int FechaAltaLaboralCellIndex = -1;
	private int FechaBajaLaboralCellIndex = -1;
	private int horasExtraForzadasCellIndex = -1;
	private int horasExtraVoluntariasCellIndex = -1;
	private int prorrataExtraCellIndex = -1;
	private int cccCellIndex = -1;
	private int ibanCellIndex = -1;
	private int paisOrigenCellIndex = -1;
	private int numTrieniosCellIndex = -1;
	private int importeBrutoCellIndex= -1;


	private Type type = Type.NIF;
	
	private XSSFWorkbook workbook;
	private XSSFSheet sheet_1;
	private XSSFSheet sheet_2;
	private String filePath;

	private static String categoriaCellString_2 = "Categoria";
	private static String salarioBaseCellString = "Salario Base";
	private static String complementosCellString = "Complementos";
	private static String codigoCotizacionCellString = "Codigo cotizacion";
	private static String brutoAnualCellString = "Bruto anual";
	private static String retencionCellString = "Retenci�n";

	private static int  categoria_2CellIndex = -1;
	private static int  salarioBaseCellIndex = -1;


	private static int  complementosCellIndex = -1;
	private static int  codigoCotizacionCellIndex = -1;
	private static int  brutoAnualCellIndex = -1;
	private static int  retencionCellIndex = -1;



	public ExcelReader(String filePath) throws IOException{
		this.filePath = filePath;
		FileInputStream file = new FileInputStream(new File(filePath));
		workbook = new XSSFWorkbook(file);

		sheet_1 = workbook.getSheetAt(0);
		sheet_2 = workbook.getSheetAt(1);


		Row header = sheet_1.getRow(0);
		int numberOfColumns = header.getLastCellNum();


		for (int i = 0; i < numberOfColumns; i++) {
			Cell currentCell = header.getCell(i,MissingCellPolicy.CREATE_NULL_AS_BLANK);

			if (currentCell.getStringCellValue().equals(cccCellString))
				cccCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(ibanCellString))
				ibanCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(emailCellString))
				emailCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(categoriaCellString))
				categoriaCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(nameCellString))
				nameCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(lastName1CellString))
				lastName1CellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(lastName2CellString))
				lastName2CellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(companyString))
				companyCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(paisOrigenString))
				paisOrigenCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(nifNieCellString))
				nifNieCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(cifCellString))
				cifCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(FechaAltaEmpresaCellString))
				FechaAltaEmpresaCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(FechaAltaLaboralCellString))
				FechaAltaLaboralCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(FechaBajaLaboralCellString))
				FechaBajaLaboralCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(horasExtraForzadasCellString))
				horasExtraForzadasCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(horasExtraVoluntariasCellString))
				horasExtraVoluntariasCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(prorrataExtraCellString))
				prorrataExtraCellIndex = currentCell.getColumnIndex();
		}

		Row header2 = sheet_2.getRow(0);
		int numberOfColumns2 = header2.getLastCellNum();

		for (int i = 0; i < numberOfColumns2; i++) {
			Cell currentCell = header2.getCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			if (currentCell.getStringCellValue().equals(categoriaCellString_2))
				categoria_2CellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(salarioBaseCellString))
				salarioBaseCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(complementosCellString))
				complementosCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(codigoCotizacionCellString))
				codigoCotizacionCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(brutoAnualCellString))
				brutoAnualCellIndex = currentCell.getColumnIndex();
			else if (currentCell.getStringCellValue().equals(retencionCellString))
				retencionCellIndex = currentCell.getColumnIndex();
		}

		Row headerTrienios = sheet_2.getRow(17);
	for (int i = 0; i < numberOfColumns2; i++){
		Cell currentCell = headerTrienios.getCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
		if(currentCell.getCellTypeEnum() != CellType.NUMERIC){
			if(currentCell.getStringCellValue().equals(numTrieniosCellString)) {
				numTrieniosCellIndex = currentCell.getColumnIndex();

			}
			else if(currentCell.getStringCellValue().equals(imorteBrutoCellString)){
				importeBrutoCellIndex = currentCell.getColumnIndex();
			}
		}
	}

		inicializarArrayListsEstaticos();

	}


	private void inicializarArrayListsEstaticos(){
		for (int j = 1; j < 16; j++) {
			Cell categoryCell = sheet_2.getRow(j).getCell(categoria_2CellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell salarioBaseCell = sheet_2.getRow(j).getCell(salarioBaseCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell complementosCell = sheet_2.getRow(j).getCell(complementosCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell codigoCotizacionCell = sheet_2.getRow(j).getCell(codigoCotizacionCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);

			categorias.add(new Category(
					categoryCell.getStringCellValue(),
					salarioBaseCell.getNumericCellValue(),
					complementosCell.getNumericCellValue(),
					codigoCotizacionCell.getNumericCellValue()
			));

		}
		for(int j = 17; j < 25; j++){
			Cell cuotaNameCell = sheet_2.getRow(j).getCell(categoria_2CellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell cuotaValorCell = sheet_2.getRow(j).getCell(categoria_2CellIndex + 1, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			String nombreCuota = cuotaNameCell.getStringCellValue();
			double valor = cuotaValorCell.getNumericCellValue();
			cuotasEmpresa.add(new CuotaEmpresa(nombreCuota,valor));
		}

		valoresTrienio[0] = 0;
		int cont = 1;
		for(int j = 17; j < 36; j++){

			//Cell numTrieniosCell = sheet_2.getRow(j).getCell(numTrieniosCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell importeBrutoCell = sheet_2.getRow(j).getCell(importeBrutoCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			if(importeBrutoCell.getCellTypeEnum() != CellType.STRING) {
				valoresTrienio[cont] = importeBrutoCell.getNumericCellValue();
				cont++;
			}
		}

		for (int j = 0; j < 50; j++) {
			Cell brutoAnualCell =  sheet_2.getRow(j).getCell(brutoAnualCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell retencionCell =  sheet_2.getRow(j).getCell(retencionCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			if(brutoAnualCell.getCellTypeEnum() != CellType.STRING && retencionCell.getCellTypeEnum() != CellType.STRING ) {
				brutoRetenciones.add(new BrutoRetenciones(brutoAnualCell.getNumericCellValue(), retencionCell.getNumericCellValue()));
			}
		}
	}
	/***
	 * Este metodo valida tanto los NIFs como los NIEs
	 * @param sheet
	 * @param wantedCellIndex
	 * @throws IOException
	 */
	public void validate(XSSFSheet sheet, int wantedCellIndex) throws IOException{

		String upperCase = "";
		for (int j = 1; j < sheet.getPhysicalNumberOfRows(); j++) {
			Cell idCell = sheet.getRow(j).getCell(wantedCellIndex);
			if(idCell == null || idCell.getCellType() == Cell.CELL_TYPE_BLANK){// Est� vacia
				addToWorkerList(sheet.getRow(j));
			}else {
				Row currentRow = idCell.getRow();
				String idCellString = idCell.getStringCellValue();
				if (Character.isLetter(idCellString.charAt(0))) {                // Si la primera es una letra entonces es un NIE
					type = Type.NIE;
				} else {
					type = Type.NIF;
				}

				if (type == Type.NIF) {
					if (!validateNIF(sheet.getRow(j),idCellString, upperCase)) {
						recalculateNIF(idCellString, idCell);
					}
				}
				if (type == Type.NIE) {
					if (!validateNIE(sheet.getRow(j),idCellString)) {
						recalculateNIE(idCellString,idCell);
					}
				}
			}
		}

		for (int j = 1; j < sheet.getPhysicalNumberOfRows(); j++) {
			Cell idCell = sheet.getRow(j).getCell(wantedCellIndex);
			if(idCell == null || idCell.getCellType() == Cell.CELL_TYPE_BLANK){// Est� vacia
				addToWorkerList(sheet.getRow(j));
			}else {
				String idCellString = idCell.getStringCellValue();
				//Ver si el NIF o NIE esta repetido.
				if (NIFsNIEs.contains(idCellString)) {
					//Si esta repetido a�adimos el NIF/NIE a el arrayList de repetidos
					NIFsNIEsRepetidos.add(idCellString);
				} else {
					//Si no lo est� lo a�adimos al arrayList de no repetidos.
					NIFsNIEs.add(idCellString);
				}

				if (checkRepeated(NIFsNIEsRepetidos, idCellString)) {
					addToWorkerList(sheet.getRow(j));
				}
			}
		}


		FileOutputStream fileOut = new FileOutputStream(filePath);
		sheet.getWorkbook().write(fileOut);
		fileOut.close();
		repeatedWorkers = deleteDuplicates(repeatedWorkers);
		XMLWriter xml = new XMLWriter(repeatedWorkers);
	}

	/***
	 * Este metodo se encarga de generar y escribir los emails para cada trabajador.
	 * @param emailCellIndex
	 * @param nameCellIndex
	 * @param lastName1CellIndex
	 * @param lastName2CellIndex
	 * @param companyCellIndex
	 * @throws IOException
	 */
	public void generateEmail(int emailCellIndex, int nameCellIndex, 
			int lastName1CellIndex, int lastName2CellIndex, int companyCellIndex) throws IOException
	{
		String email = "";
		int emailPartition = 2;
		int counter = 0;
		boolean isModified = false;


		for (int i = 1; i < sheet_1.getPhysicalNumberOfRows(); i++) {
			Cell emailCell = sheet_1.getRow(i).getCell(emailCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			String emailCellString = emailCell.getStringCellValue();

			String name = sheet_1.getRow(i).getCell(nameCellIndex).getStringCellValue();
			name = Normalizer.normalize(name, Normalizer.Form.NFD);
			name = name.replaceAll("[^\\p{ASCII}]", "");

			String lastName1 = sheet_1.getRow(i).getCell(lastName1CellIndex).getStringCellValue();
			lastName1 = lastName1.toLowerCase();
			lastName1 = Normalizer.normalize(lastName1, Normalizer.Form.NFD);
			lastName1 = lastName1.replaceAll("[^\\p{ASCII}]", "");

			String lastName2 = sheet_1.getRow(i).getCell(lastName2CellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
			lastName2 = lastName2.toLowerCase();
			lastName2 = Normalizer.normalize(lastName2, Normalizer.Form.NFD);
			lastName2 = lastName2.replaceAll("[^\\p{ASCII}]", "");

			String company = sheet_1.getRow(i).getCell(companyCellIndex).getStringCellValue();
			company = company.replaceAll("\\.","");
			company = company.replaceAll(" ","").toLowerCase();

			String letters;
			if(lastName2.length() < emailPartition) {
				letters = name.substring(0,emailPartition) + lastName1.substring(0,emailPartition);
			}else {
				letters = name.substring(0,emailPartition) + lastName1.substring(0,emailPartition) + lastName2.substring(0,emailPartition);
			}

			if(emailCellString.length() >= letters.length() && emailCellString.substring(0, letters.length()).equals(letters))
				break;

			for (int j = 1; j < sheet_1.getPhysicalNumberOfRows(); j++) {
				String emailString = sheet_1.getRow(j).getCell(emailCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();

				if(emailString.length() >= letters.length() && emailString.substring(0, letters.length()).equals(letters)) {
					counter++;
				}
			}

			//Mira si el ususari esta repetido
			String repetCounterString = String.valueOf(counter);

			if(repetCounterString.length() == 1)
				repetCounterString = "0" + repetCounterString;

			email = letters	+ repetCounterString + "@" + company + ".es";

			sheet_1.getRow(i).getCell(emailCellIndex).setCellValue(email);

			isModified = true;
			counter = 0;
		}

		FileOutputStream fileOut = new FileOutputStream(filePath);
		sheet_1.getWorkbook().write(fileOut);
		fileOut.close();

		if(isModified)
			System.out.println("Emails generados correctamente.");
		else
			System.out.println("No se ha generado ning�n email nuevo.");
	}

	/***
	 * Valida la cuenta corriente del trabajador
	 * @param cccCellIndex
	 * @throws IOException
	 */
	public void validateCCC( int cccCellIndex) throws IOException{
		//
		//Recibe o lee el C�digo Cuenta (Columna O / 15)
		//
		boolean isModified = false;
		StringBuilder cuentaCalculada;
		for (int i = 1; i < sheet_1.getPhysicalNumberOfRows(); i++) {

			String contenidoColumnaString = sheet_1.getRow(i).getCell(cccCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
			int[] codigoCuenta = new int[20];
			cuentaCalculada = new StringBuilder(codigoCuenta.length);
			int suma = 0;
			int d1 = -1;
			int d2 = -1;
	
			//Guardo el contenido en un array de enteros
			for(int j = 0; j < 20; j++) {
				codigoCuenta[j] = Character.getNumericValue(contenidoColumnaString.charAt(j));//parseInt("" + contenidoColumnaString.charAt(j)); 
			}
	
			//Multiplico cada posici�n del array por (2^n)mod 11 y lo sumo
			for(int j = 0; j < 8; j++) {
				suma += codigoCuenta[j] * ((Math.pow(2, j+2)) % 11);
			}
	
			//Calculo el primer d�gito de control
			d1 = suma % 11;
			if(d1 == 10) d1 = 1;
	
			suma = 0;
	
			//Multiplico cada posici�n del array por (2^n)mod 11 y lo sumo
			for(int j = 10; j < 20; j++) {
				suma += codigoCuenta[j] * ((Math.pow(2, j)) % 11);
			}
	
			//Calculo el primer d�gito de control
			d2 = suma % 11;
			if(d2 == 10) d2 = 1;
	
			if(d1 != codigoCuenta[8] || d2 != codigoCuenta[9]) {
				codigoCuenta[8]=d1;
				codigoCuenta[9]=d2;
	
				//
				//Escribe la nueva CCC corregida en el excel (Columna O / 15)
				for(int j : codigoCuenta) {
					cuentaCalculada.append(j);
				}
				if(!cuentaCalculada.toString().equals(contenidoColumnaString)) {
					sheet_1.getRow(i).getCell(cccCellIndex).setCellValue(cuentaCalculada.toString());
					isModified = true;
				}
			}
		}
		FileOutputStream fileOut = new FileOutputStream(filePath);
		sheet_1.getWorkbook().write(fileOut);
		fileOut.close();
		
		if(isModified)
			System.out.println("Se han corregido algunas cuentas corrientes.");
		else
			System.out.println("Las cuentas corrientes est�n correctas.");
	}

	/***
	 * Calcula el IBAN de cada trabajador.
	 * @param ibanCellIndex
	 * @param cccCellIndex
	 * @param paisOrigenIndex
	 * @throws IOException
	 */
	public void calculateIban(int ibanCellIndex, int cccCellIndex, int paisOrigenIndex)  throws IOException{

		boolean isModified = false;
		StringBuilder IBANCodeStringBuilder;

		for (int i = 1; i < sheet_1.getPhysicalNumberOfRows(); i++) {
			//
			//Recibe o lee el C?digo Cuenta (Columna O / 15)
			//
			String CCCString = sheet_1.getRow(i).getCell(cccCellIndex).getStringCellValue();

			BigInteger IBAN = new BigInteger(CCCString);
			//
			//Recibe o lee el C?digo Bancario (Columna P / 16)
			//
			String CodigoBancario = sheet_1.getRow(i).getCell(paisOrigenIndex).getStringCellValue();
			BigInteger noventaysiete = new BigInteger("97");
			BigInteger cien = new BigInteger("100");
			int aux;
			for(int j = 0; j < 2; j++) {
				switch(CodigoBancario.charAt(j)) {
					case 'A':
						aux=10;
						break;
					case 'B':
						aux=11;
						break;
					case 'C':
						aux=12;
						break;
					case 'D':
						aux=13;
						break;
					case 'E':
						aux=14;
						break;
					case 'F':
						aux=15;
						break;
					case 'G':
						aux=16;
						break;
					case 'H':
						aux=17;
						break;
					case 'I':
						aux=18;
						break;
					case 'J':
						aux=19;
						break;
					case 'K':
						aux=20;
						break;
					case 'L':
						aux=21;
						break;
					case 'M':
						aux=22;
						break;
					case 'N':
						aux=23;
						break;
					case 'O':
						aux=24;
						break;
					case 'P':
						aux=25;
						break;
					case 'Q':
						aux=26;
						break;
					case 'R':
						aux=27;
						break;
					case 'S':
						aux=28;
						break;
					case 'T':
						aux=29;
						break;
					case 'U':
						aux=30;
						break;
					case 'V':
						aux=31;
						break;
					case 'W':
						aux=32;
						break;
					case 'X':
						aux=33;
						break;
					case 'Y':
						aux=34;
						break;
					case 'Z':
						aux=35;
						break;
					default:
						System.out.println("Error.");
						aux = -1;
						break;
				}
				IBAN = IBAN.multiply(cien);
				String n = ""+aux;
				BigInteger auxBig = new BigInteger(n);
				IBAN = IBAN.add(auxBig);
			}
			IBAN = IBAN.multiply(cien);
			double r = (IBAN.mod(noventaysiete)).intValue();
			double control = 98-r;
			String codigoIBAN = "";


			codigoIBAN += CodigoBancario.charAt(0);
			codigoIBAN += CodigoBancario.charAt(1);
			codigoIBAN += (int)(control - control%10)/10;//Decenas
			codigoIBAN += (int)(control%10);//Unidades


			String codigo = codigoIBAN + CCCString;

			sheet_1.getRow(i).getCell(ibanCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			isModified = true;
			//}
			sheet_1.getRow(i).getCell(ibanCellIndex).setCellValue(codigo.toString());

		}

		System.out.println("Se han generado los IBAN.");
		FileOutputStream fileOut = new FileOutputStream(filePath);
		sheet_1.getWorkbook().write(fileOut);
		fileOut.close();
	}

	/***
	 * Cierra el Workbook con el que estamos trabajando.
	 */
	public void closeWorkBook() {
		try {
			this.workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***
	 * Add all Workers from the sheet_1 to the ArrayList.
	 */
	public void AddWorkersToArrayList(){
		for (int i = 1; i < sheet_1.getPhysicalNumberOfRows(); i++) {
			Cell idCellID = sheet_1.getRow(i).getCell(nifNieCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellName = sheet_1.getRow(i).getCell(nameCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellFirstSurname = sheet_1.getRow(i).getCell(lastName1CellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellSecondSurname = sheet_1.getRow(i).getCell(lastName2CellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellCategoria = sheet_1.getRow(i).getCell(categoriaCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellEmpresa = sheet_1.getRow(i).getCell(companyCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellemail = sheet_1.getRow(i).getCell(emailCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellcifEmpresa = sheet_1.getRow(i).getCell(cifCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellfechaAltaEmpresa = sheet_1.getRow(i).getCell(FechaAltaEmpresaCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellFechaAltaLaboral = sheet_1.getRow(i).getCell(FechaAltaLaboralCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellFechaBajaLaboral = sheet_1.getRow(i).getCell(FechaBajaLaboralCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellHorasExtraForzadas = sheet_1.getRow(i).getCell(horasExtraForzadasCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellHorasExtraVoluntarias = sheet_1.getRow(i).getCell(horasExtraVoluntariasCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellProrrataExtra = sheet_1.getRow(i).getCell(prorrataExtraCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellCcc = sheet_1.getRow(i).getCell(cccCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellPaisOrigenCuenta = sheet_1.getRow(i).getCell(paisOrigenCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			Cell idCellIban = sheet_1.getRow(i).getCell(ibanCellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
			int index;
			switch (idCellCategoria.getStringCellValue()){
				case "Operador":
					index = 0;
					break;
				case "Auxiliar":
					index = 1;
					break;
				case "Administrativo":
					index = 2;
					break;
				case "Jefe de secci�n":
					index = 3;
					break;
				case "Jefe divisi�n":
					index = 4;
					break;
				case "Programador":
					index = 5;
					break;
				case "Analista":
					index = 6;
					break;
				case "Jefe de servicio":
					index = 7;
					break;
				case "Limpiador":
					index = 8;
					break;
				case "Cocinero":
					index = 9;
					break;
				case "Cuidador":
					index = 10;
					break;
				case "Ordenanza":
					index = 11;
					break;
				case "Calefactor":
					index = 12;
					break;
				case "Coordinador":
					index = 13;
					break;
				default:
					index = 0;
					break;
			}


			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String fae = "";
			String fal = "";
			String fbl = "";
			if(idCellfechaAltaEmpresa.getDateCellValue() != null)
				fae = sdf.format(idCellfechaAltaEmpresa.getDateCellValue());
			if(idCellFechaAltaLaboral.getDateCellValue() != null)
				fal = sdf.format(idCellFechaAltaLaboral.getDateCellValue());
			if(idCellFechaBajaLaboral.getDateCellValue() != null)
				fbl = sdf.format(idCellFechaBajaLaboral.getDateCellValue());

			Worker w = new Worker(idCellID.getStringCellValue(),
					idCellName.getStringCellValue(),
					idCellFirstSurname.getStringCellValue(),
					idCellSecondSurname.getStringCellValue(),
					categorias.get(index),
					idCellEmpresa.getStringCellValue(),
					idCellemail.getStringCellValue(),
					idCellcifEmpresa.getStringCellValue(),
					fae,
					fal,
					fbl,
					String.valueOf(idCellHorasExtraForzadas.getNumericCellValue()),
					String.valueOf(idCellHorasExtraVoluntarias.getNumericCellValue()),
					idCellProrrataExtra.getStringCellValue(),
					idCellCcc.getStringCellValue(),
					idCellPaisOrigenCuenta.getStringCellValue(),
					idCellIban.getStringCellValue());

			allWorkers.add(w);

		}
		deleteDuplicates(allWorkers);
		//for (int i = 0; i < allWorkers.size(); i++){
	//		System.out.println("Worker " + (i+1) + " : " + allWorkers.get(i).getNombre() + " " + allWorkers.get(i).getPrimerApellido() + " " + allWorkers.get(i).getSegundoApellido());
		//}
	}
	/***
	 * Elimina trabajadores duplicados en el arraylist de trabajadores
	 * @param repeatedWorkers
	 * @return
	 */
	private ArrayList<Worker> deleteDuplicates(ArrayList<Worker> repeatedWorkers) {
		for(int i = 0; i < repeatedWorkers.size(); i++) {
			for(int j = i + 1; j < repeatedWorkers.size(); j++) {
				if(repeatedWorkers.get(i).checkEqual(repeatedWorkers.get(j))){
					repeatedWorkers.remove(j);
					j--;
				}
			}
		}
		return repeatedWorkers;
	}


	/***
	 * Recalcula un NIF valido.
	 * @param _nif
	 * @param _idCell
	 */
	private void recalculateNIF(String _nif, Cell _idCell) {
		char letters[] ={'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B',
				'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
		String resultado = "";
		String numero = _nif.substring(0,8);

		int num = Integer.parseInt(numero);
		int resto = num % 23;
		char letra = letters[resto];
		resultado = numero + letra;

		_idCell.setCellValue(resultado);

	}

	/***
	 * Recalcula un NIE valido.
	 * @param _nie
	 * @param _idCell
	 */
	private void recalculateNIE(String _nie, Cell _idCell) {
		char letters[] ={'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B',
				'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
		String resultado = "";
		String numero = _nie.substring(0,8);
		String aux = "";

		int num;
		int resto;
		char letra;


		switch (numero.charAt(0)){
			case 'X':
				aux += '0';
				break;
			case 'Y':
				aux += '1';
				break;
			case 'Z':
				aux += '2';
				break;
		}
		aux += numero.substring(1,8);
		num = Integer.parseInt(aux);
		resto = num % 23;
		letra = letters[resto];
		resultado = numero.charAt(0) + numero.substring(1,8) + letra;

		_idCell.setCellValue(resultado);



	}

	/***
	 * Metodo para validar NIFs
	 * @param row
	 * @param NIF
	 * @param upperCase
	 * @return
	 * @throws IOException
	 */
	private boolean validateNIF(XSSFRow row,String NIF, String upperCase) throws IOException{

		if(NIF.length() != 9 || Character.isLetter(NIF.charAt(8)) == false){
			handleError();
			return false;
		}

		if(Character.isLowerCase(NIF.charAt(0)))
			upperCase = NIF.substring(0).toUpperCase();
		else
			upperCase = NIF.substring(0);

		if(!onlyNumbers(NIF) && !nifLetter(NIF).equals(upperCase)){
			handleError();
			return false;
		}

		return true;
	}

	/***
	 * Metodo para validar NIEs
	 * @param row
	 * @param NIE
	 * @return
	 */
	private boolean validateNIE(XSSFRow row,String NIE){
		boolean isValid = true;
		int pos = 1;
		int ASCIIChar;
		char letter;
		int myNIE;
		int reminder;
		char letters[] ={'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B',
				'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};

		if (NIE.length() == 9 && Character.isLetter(NIE.charAt(8))
				&& (Character.toUpperCase(NIE.charAt(0)) == 'X'
				|| Character.toUpperCase(NIE.charAt(0)) == 'Y'
				|| Character.toUpperCase(NIE.charAt(0)) == 'Z')) {
			do {
				ASCIIChar = NIE.codePointAt(pos);
				isValid = (ASCIIChar > 47 && ASCIIChar < 58);
				pos++;
			} while (pos < NIE.length() - 1 && isValid);
		}

		if (isValid && NIE.substring(0, 1).toUpperCase().equals("X")) {
			NIE = "0" + NIE.substring(1, 9);
		} else if (isValid && NIE.substring(0, 1).toUpperCase().equals("Y")) {
			NIE = "1" + NIE.substring(1, 9);
		} else if (isValid && NIE.substring(0, 1).toUpperCase().equals("Z")) {
			NIE = "2" + NIE.substring(1, 9);
		}

		if (isValid) {
			letter = Character.toUpperCase(NIE.charAt(8));
			myNIE = Integer.parseInt(NIE.substring(1, 8));
			reminder = myNIE % 23;
			isValid = (letter == letters[reminder]);
		}

		return isValid;
	}

	/***
	 * Handle Error
	 * @throws IOException
	 */
	private void handleError() throws IOException{
		FileOutputStream fileOut = new FileOutputStream(filePath);
		sheet_1.getWorkbook().write(fileOut);
		fileOut.close();
	}

	/***
	 * Metodo para comprobbar lso numeros del CCC
	 * @param idCellString
	 * @return
	 */
	private boolean onlyNumbers(String idCellString){
		String number;
		StringBuilder NIF = new StringBuilder();
		String[] numbers = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
		for (int i = 0; i < idCellString.length() - 1; i++) {
			number = idCellString.substring(i, i + 1);

			for (String number1 : numbers) {
				if (number.equals(number1)) {
					NIF.append(number1);
				}
			}
		}

		return NIF.length() == 8;

	}

	/***
	 * Metodo para calcular las Letras del NIF
	 * @param idCellString
	 * @return
	 */
	private String nifLetter(String idCellString){
		String letters[] ={"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B",
				"N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"};
		int reminder;
		String letter = "";
		int NIFNumber = Integer.parseInt(idCellString.substring(0, 8));


		reminder = NIFNumber % 23;
		letter = letters[reminder];

		return letter;
	}
	//Aniade trabajadores al arraylist de trabajadores.
	private void addToWorkerList(XSSFRow row){

		repeatedWorkers.add(new Worker(String.valueOf(row.getRowNum()), row.getCell(3).getStringCellValue(), row.getCell(2).getStringCellValue(),
				row.getCell(1).getStringCellValue(), row.getCell(5).getStringCellValue(), row.getCell(6).getStringCellValue()));
	}

	/***
	 * Mira si el arraylist contiene un string
	 * @param arr
	 * @param targetValue
	 * @return
	 */
	private static boolean checkRepeated(ArrayList<String> arr, String targetValue){
		return arr.contains(targetValue);
	}

	public int getLastName1CellIndex() {
		return lastName1CellIndex;
	}

	public int getLastName2CellIndex() {
		return lastName2CellIndex;
	}

	public int getNameCellIndex() {
		return nameCellIndex;
	}

	public int getEmailCellIndex() {
		return emailCellIndex;
	}

	public int getCompanyCellIndex() {
		return companyCellIndex;
	}

	public int getCccCellIndex() {
		return cccCellIndex;
	}

	public int getIbanCellIndex() {
		return ibanCellIndex;
	}

	public int getPaisOrigenCellIndex() {
		return paisOrigenCellIndex;
	}

	public XSSFSheet getSheet_1() {
		return sheet_1;
	}
	public int getNifNieCellIndex() {
		return nifNieCellIndex;
	}
	public ArrayList<Worker> getAllWorkers() {
		return allWorkers;
	}

	public static ArrayList<BrutoRetenciones> getBrutoRetenciones() {
		return brutoRetenciones;
	}

	public static ArrayList<CuotaEmpresa> getCuotasEmpresa() {
		return cuotasEmpresa;
	}

	public static double[] getValoresTrienio() {
		return valoresTrienio;
	}

	public static int getCategoria_2CellIndex() {
		return categoria_2CellIndex;
	}

	public static int getSalarioBaseCellIndex() {
		return salarioBaseCellIndex;
	}

	public static int getComplementosCellIndex() {
		return complementosCellIndex;
	}

	public static int getCodigoCotizacionCellIndex() {
		return codigoCotizacionCellIndex;
	}

	public static int getBrutoAnualCellIndex() {
		return brutoAnualCellIndex;
	}

	public static int getRetencionCellIndex() {
		return retencionCellIndex;
	}

	public ArrayList<Category> getCategorias() {
		return categorias;
	}
}
