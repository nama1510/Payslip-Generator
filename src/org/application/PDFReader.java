package org.application;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import org.data.CuotaEmpresa;
import org.data.Nomina;
import org.data.Worker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PDFReader {

    String name;
    String nifNie;
    String empresa;
    String cifEmpresa;
    String direccion;
    String iban;
    String nombreCategoria;
    String brutoAnual;
    String brutoMes;
    String fechaAlta;

    String salarioBase;
    String prorrata;
    String complementos;
    String antiguedad; //trienios
    String trieniosValor;

    String contingenciasGenerales;
    String desempleo;
    String cuotaFormacion;
    String IRPF;

    String totalDeducciones;
    String totalDevengos;
    String liquidoAPercibir;

    String retencion;
    String deduccionIRPF;

    String mesString;
    String anioString;

    String desempleo_emp , formacion_Emp, accidentes, costeTotalTrabajador, fogasa, totalEmpresario;

    ArrayList<CuotaEmpresa> cuotaEmpresa;
    boolean isExtra;
    int diasMes;
    Document document;

    Nomina nomina;
    public static final String IMG = "resources\\images\\Nomina_image.png";
    public PDFReader(Worker worker, String liquidoAPercibir, String prorrata, String complementos, String antiguedad, String trieniosValor, String brutoMes, String ss, String desempleo,
                     String  formacion, String irpf, String retencion, String mesString, String anioString, String contingenciasComunes, String fogasa, String desempleo_Emp,
                     String formacion_Emp, String accidentes, String costeTotalsTrabajador, String totalEmpresario, boolean isExtra, ArrayList<CuotaEmpresa> cuotaEmpresa, String brutoAnual) {

        this.cuotaEmpresa = cuotaEmpresa;
        this.isExtra = isExtra;
        this.desempleo_emp = desempleo_Emp;
        this.formacion_Emp = formacion_Emp;
        this.accidentes = accidentes;
        this.costeTotalTrabajador = costeTotalsTrabajador;
        this.fogasa = fogasa;
        this.totalEmpresario = totalEmpresario;
        nomina = new Nomina();

        name = worker.getNombre() + " " + worker.getPrimerApellido() + " " +worker.getSegundoApellido();
        nifNie = worker.getId();
        empresa = worker.getEmpresa();
        cifEmpresa = worker.getCifEmpresa();
        iban = worker.getIban();
        nombreCategoria = worker.getCategoria().getName();
        if(!worker.isProrrataExtra()) {
            salarioBase = String.valueOf(round2Decimals(worker.getCategoria().getSalarioBase() / 14));
        }else{
            salarioBase = String.valueOf(round2Decimals(worker.getCategoria().getSalarioBase() / 12));
        }
        fechaAlta = worker.getFechaAltaEmpresa();

        this.mesString = mesString;
        this.anioString = anioString;
        this.liquidoAPercibir = liquidoAPercibir;
        this.prorrata = prorrata;
        this.complementos = String.valueOf( round2Decimals(Double.parseDouble(complementos) /12));
        this.antiguedad = antiguedad;
        this.trieniosValor = trieniosValor;
        this.brutoMes = String.valueOf( round2Decimals(Double.parseDouble( brutoMes ) ));
        this.contingenciasGenerales = String.valueOf( round2Decimals(Double.parseDouble(ss)));
        this.desempleo = String.valueOf( round2Decimals(Double.parseDouble(desempleo)));
        this.cuotaFormacion = String.valueOf( round2Decimals(Double.parseDouble(formacion)));
        this.retencion = String.valueOf( retencion);
        this.deduccionIRPF = irpf;

        this.brutoAnual = brutoAnual;
        nomina.setBrutoAnual(round2Decimals( Double.parseDouble(brutoAnual)));

        if(mesString == "Abril" || mesString == "Junio" || mesString == "Septiembre" || mesString == "Noviembre"){
            diasMes = 30;
        }else if(mesString == "Febrero"){
            if(Integer.parseInt(anioString)%4 == 0) {
                diasMes = 29;
            }else if(Integer.parseInt(anioString)%100 == 0 && Integer.parseInt(anioString)%400 != 0){
                diasMes = 29;
            }else{
                diasMes = 28;
            }
        }else{
            diasMes = 31;
        }

        //Crear Documento
        document = new Document();
        File directorioAnio = new File("resources\\nominas\\" + anioString);
        directorioAnio.mkdir();
        File directorioMes = new File("resources\\nominas\\" + anioString + "\\" + mesString);
        directorioMes.mkdir();

        try {
            if(!isExtra) {
                if (worker.getSegundoApellido().equals(""))
                    PdfWriter.getInstance(document, new FileOutputStream("resources\\nominas\\" + anioString + "\\" + mesString + "\\" + worker.getId() + "_" + worker.getNombre() + worker.getPrimerApellido() + "_" + mesString + anioString + ".pdf"));
                else
                    PdfWriter.getInstance(document, new FileOutputStream("resources\\nominas\\" + anioString + "\\" + mesString + "\\" + worker.getId() + "_"+ worker.getNombre()  + worker.getPrimerApellido() + worker.getSegundoApellido() + "_" + mesString + anioString + ".pdf"));
            }else{
                File directorioExtra = new File("resources\\nominas\\" + anioString + "\\" + mesString + "\\Extra");
                directorioExtra.mkdir();
                if (worker.getSegundoApellido().equals(""))
                    PdfWriter.getInstance(document, new FileOutputStream("resources\\nominas\\" + anioString + "\\" + mesString + "\\Extra\\" + worker.getId() + "_" + worker.getNombre() + worker.getPrimerApellido() + "_" + mesString + anioString + "_Extra" + ".pdf"));
                else
                    PdfWriter.getInstance(document, new FileOutputStream("resources\\nominas\\" + anioString + "\\" + mesString + "\\Extra\\" + worker.getId() + "_" + worker.getNombre() + worker.getPrimerApellido() + worker.getSegundoApellido() + "_" + mesString + anioString + "_Extra" + ".pdf"));

            }
            document.open();


            document.add(createTable1());
            document.add(createTable2());
            document.add(createTable3());
            document.add(createTable4());

            document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private double round2Decimals(double num){
        return Math.round(num * 100.0)/100.0;
    }
    private PdfPTable createTable1() throws DocumentException {
        PdfPTable table1 =  new PdfPTable(2);
        Paragraph p1 = new Paragraph(empresa + "\n" + "Cif: " + cifEmpresa);
        Paragraph p2 = new Paragraph("IBAN: " + iban +"\n"+ "Categoria: " + nombreCategoria + "\n" + "Bruto Anual: " + brutoAnual + "\n" + "Fecha de alta: " + fechaAlta);

        PdfPCell cell1 = new PdfPCell();
        PdfPCell cell2 = new PdfPCell();
        cell2.setBorder(Rectangle.NO_BORDER);
        cell1.setBackgroundColor(new BaseColor(255, 204, 128));

        cell2.setColspan(3);

        cell1.addElement(p1);
        cell2.addElement(p2);

        cell1.setPadding(5f);
        cell2.setPadding(5f);

        table1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table1.addCell(cell1);
        table1.addCell(cell2);

        table1.setSpacingAfter(20f);
        table1.setSpacingBefore(20f);

        return table1;
    }

    private PdfPTable createTable2() throws DocumentException, IOException {

        Image image = Image.getInstance(IMG);
        image.scalePercent(20,20);

        PdfPTable table2 = new PdfPTable(2);

        Paragraph p3 = new Paragraph("Destinatario:\n\t\t\t" + name + "\n\t\t\t" +"Nif/Nie: " + nifNie);

        PdfPCell cell1 = new PdfPCell();
        PdfPCell cell2 = new PdfPCell();

        cell2.setBorder(Rectangle.NO_BORDER);
        cell1.setBackgroundColor(new BaseColor(255, 204, 128));


        cell1.addElement(p3);
        cell2.addElement(image);

        cell1.setPadding(5f);
        cell2.setPadding(5f);

        table2.setSpacingBefore(10f);
        table2.setSpacingAfter(20f);

        table2.setHorizontalAlignment(Element.ALIGN_CENTER);
        table2.addCell(cell2);
        table2.addCell(cell1);

        return table2;
    }

    private PdfPTable createTable3() throws DocumentException{

        PdfPTable tableTitle = new PdfPTable(5);
        tableTitle.setWidthPercentage(100);
        tableTitle.getDefaultCell().setBorder(0);// = 0;

        Font font = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        Paragraph title;
        if(!isExtra)
            title = new Paragraph("Nomina " + mesString + " " + anioString, font);
        else
            title = new Paragraph("Nomina " + mesString + " " + anioString + " EXTRA", font);

        title.setLeading(0, 1);

        PdfPCell cell1 = new PdfPCell(title); //FIXME: Cambiarlo a la fecha en la que se genera la nomina.
        cell1.setVerticalAlignment(Element.ALIGN_CENTER);
        cell1.setColspan(5);
        cell1.setMinimumHeight(50);

        PdfPCell cell2 = new PdfPCell(new Phrase(""));
        PdfPCell cell3 = new PdfPCell(new Phrase("cant."));
        PdfPCell cell4 = new PdfPCell(new Phrase("Inmp."));
        PdfPCell cell5 = new PdfPCell(new Phrase("Dev."));
        PdfPCell cell6 = new PdfPCell(new Phrase("Deducc."));

        double salarioBaseMes = round2Decimals(Double.parseDouble(salarioBase));
        double valorProrateo = round2Decimals( Double.parseDouble(prorrata));
        double complementoMes = round2Decimals(Double.parseDouble(complementos) );
        double importeTrienios = round2Decimals(Double.parseDouble(complementos));
        double importeSegSocialTrabajador = Double.parseDouble(contingenciasGenerales);
        double importeDesempleoTrabajador = Double.parseDouble(desempleo);
        double importeFormacionTrabajador = round2Decimals(Double.parseDouble(cuotaFormacion));
        double importeIRPF = round2Decimals(Double.parseDouble(deduccionIRPF));
        double brutoNomina = round2Decimals(Double.parseDouble(salarioBase) + Double.parseDouble(prorrata) + Double.parseDouble(complementos) + Double.parseDouble(trieniosValor) );
        double lquidoAPercibi = round2Decimals((Double.parseDouble(salarioBase) + Double.parseDouble(prorrata) + Double.parseDouble(complementos) + Double.parseDouble(trieniosValor)) -
                (Double.valueOf(contingenciasGenerales) + Double.valueOf(desempleo) + Double.valueOf(cuotaFormacion) + Double.valueOf(deduccionIRPF)));

        double numTrienios = Double.parseDouble(antiguedad);
        double segSocialTrabajador = cuotaEmpresa.get(0).getValor();
        double desempleoTrabajador = cuotaEmpresa.get(1).getValor();
        double cuotaFormacionTrabajador = cuotaEmpresa.get(2).getValor();
        double irpfTrabajador  = Double.parseDouble(retencion);

        nomina.setNumTrienios(numTrienios);
        nomina.setSegSocialTrabajador(segSocialTrabajador);
        nomina.setDesempleoTrabajador( desempleoTrabajador);
        nomina.setFormacionTrabajador(cuotaFormacionTrabajador);
        nomina.setIrpf(irpfTrabajador);

        nomina.setImporteSalarioMes(salarioBaseMes);
        nomina.setBrutoNomina(brutoNomina);
        nomina.setValorProrrateo(valorProrateo);
        nomina.setImporteComplementoMes(complementoMes);
        nomina.setImporteTrienios(importeTrienios);
        nomina.setImporteSegSocialTrabajador(importeSegSocialTrabajador);
        nomina.setImporteDesempleoTrabajador(importeDesempleoTrabajador);
        nomina.setImporteFormacionTrabajador(importeFormacionTrabajador);
        nomina.setImporteIrpf(importeIRPF);
        nomina.setLiquidoNomina(lquidoAPercibi);



        PdfPCell cellSalarioBase   = new PdfPCell(new Phrase("Salario Base"));
            PdfPCell c1sb = new PdfPCell(new Phrase(String.valueOf(diasMes) + " Dias"));
            PdfPCell c2sb = new PdfPCell(new Phrase(String.valueOf( round2Decimals(Double.parseDouble(salarioBase)/diasMes))));
            PdfPCell c3sb = new PdfPCell(new Phrase(salarioBase));
            PdfPCell c4sb = new PdfPCell(new Phrase());
        PdfPCell cellProrrata      = new PdfPCell(new Phrase("Prorrata"));
            PdfPCell c1p = new PdfPCell(new Phrase(String.valueOf(diasMes) + " Dias"));
            PdfPCell c2p = new PdfPCell(new Phrase(String.valueOf( round2Decimals( Double.parseDouble(prorrata)/diasMes) )));
            PdfPCell c3p = new PdfPCell(new Phrase(String.valueOf( round2Decimals( Double.parseDouble(prorrata)))));
            PdfPCell c4p = new PdfPCell(new Phrase());
        PdfPCell cellComplemento   = new PdfPCell(new Phrase("Complemento"));
            PdfPCell c1com = new PdfPCell(new Phrase(String.valueOf(diasMes) + " Dias"));
            PdfPCell c2com = new PdfPCell(new Phrase(String.valueOf( round2Decimals(Double.parseDouble(complementos)/diasMes ))));
            PdfPCell c3com = new PdfPCell(new Phrase(complementos));
            PdfPCell c4com = new PdfPCell(new Phrase());
        PdfPCell cellAntiguedad    = new PdfPCell(new Phrase("Antiguedad"));
            PdfPCell c1ant = new PdfPCell(new Phrase(antiguedad + " Trienios"));
            PdfPCell c2ant = new PdfPCell(new Phrase(String.valueOf( round2Decimals(Double.parseDouble(trieniosValor)/diasMes) )));
            PdfPCell c3ant = new PdfPCell(new Phrase(trieniosValor));
            PdfPCell c4ant = new PdfPCell(new Phrase());
        PdfPCell cellContingencias = new PdfPCell(new Phrase("Contingencias Generales"));
            PdfPCell c1cont = new PdfPCell(new Phrase( cuotaEmpresa.get(0).getValor() + "%"));
            PdfPCell c2cont = new PdfPCell(new Phrase("de " + (!isExtra ? brutoMes : "0")));
            PdfPCell c3cont = new PdfPCell(new Phrase());
            PdfPCell c4cont = new PdfPCell(new Phrase(contingenciasGenerales));
        PdfPCell cellDesempleo     = new PdfPCell(new Phrase("Desempleo"));
            PdfPCell c1des = new PdfPCell(new Phrase(cuotaEmpresa.get(1).getValor()  + "%"));
            PdfPCell c2des = new PdfPCell(new Phrase("de " + (!isExtra ? brutoMes : "0")));
            PdfPCell c3des = new PdfPCell(new Phrase());
            PdfPCell c4des = new PdfPCell(new Phrase(desempleo));
        PdfPCell cellCuotaForm     = new PdfPCell(new Phrase("Cuota formacion"));
            PdfPCell c1cuot = new PdfPCell(new Phrase(cuotaEmpresa.get(2).getValor() + "%"));
            PdfPCell c2cuot = new PdfPCell(new Phrase("de " + (!isExtra ? brutoMes : "0")));
            PdfPCell c3cuot = new PdfPCell(new Phrase());
            PdfPCell c4cuot = new PdfPCell(new Phrase(String.valueOf( round2Decimals(Double.parseDouble(cuotaFormacion)))));
        PdfPCell cellIRPF          = new PdfPCell(new Phrase("IRPF"));
            PdfPCell c1irpf = new PdfPCell(new Phrase(retencion + "%"));
            PdfPCell c2irpf = new PdfPCell(new Phrase("de " + brutoMes));
            PdfPCell c3irpf = new PdfPCell(new Phrase());
            PdfPCell c4irpf = new PdfPCell(new Phrase(String.valueOf( round2Decimals(Double.parseDouble(deduccionIRPF)))));

        PdfPCell deducciones = new PdfPCell(new Phrase("Total Deducciones"));
            PdfPCell c1deduc = new PdfPCell(new Phrase());
            c1deduc.setColspan(3);
            PdfPCell c4deduc = new PdfPCell(new Phrase(String.valueOf( round2Decimals(Double.valueOf(contingenciasGenerales) + Double.valueOf(desempleo) + Double.valueOf(cuotaFormacion) + Double.valueOf(deduccionIRPF) ))));
        PdfPCell Devengos    = new PdfPCell(new Phrase("Total Devengos"));
            PdfPCell c1dev = new PdfPCell(new Phrase());
            c1dev.setColspan(2);
            PdfPCell c3dev = new PdfPCell(new Phrase(String.valueOf( round2Decimals(Double.parseDouble(salarioBase) + Double.parseDouble(prorrata) + Double.parseDouble(complementos) + Double.parseDouble(trieniosValor) )) ));
            PdfPCell c4dev = new PdfPCell(new Phrase());
            c4dev.setBorder(Rectangle.NO_BORDER);
        PdfPCell liquidoAPecibir    = new PdfPCell(new Phrase("Liquido a percibir"));
            PdfPCell c1iliqu = new PdfPCell(new Phrase());
            c1iliqu.setBorder(Rectangle.NO_BORDER);
            PdfPCell c2iliqu = new PdfPCell(new Phrase());
            c2iliqu.setBorder(Rectangle.NO_BORDER);
            PdfPCell c3iliqu = new PdfPCell(new Phrase());
            c3iliqu.setBorder(Rectangle.NO_BORDER);
            PdfPCell c4iliqu = new PdfPCell(new Phrase(String.valueOf( round2Decimals((Double.parseDouble(salarioBase) + Double.parseDouble(prorrata) + Double.parseDouble(complementos) + Double.parseDouble(trieniosValor)) - (Double.valueOf(contingenciasGenerales) + Double.valueOf(desempleo) + Double.valueOf(cuotaFormacion) + Double.valueOf(deduccionIRPF))/*liquidoAPercibir*/))));//FIXME: Calcularlo en el calculador de nominas y pasarle el valor
            c4iliqu.setBorder(Rectangle.NO_BORDER);



        cell1.setBorder(Rectangle.NO_BORDER);
        cell1.setBackgroundColor(new BaseColor(255, 204, 128));


        //cell1.addElement(p1);
        //cell2.addElement(p2);

        cell1.setPadding(5f);
        cell2.setPadding(5f);

        tableTitle.setSpacingBefore(10f);
        tableTitle.setSpacingAfter(20f);

        tableTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableTitle.addCell(cell1);
        tableTitle.addCell(cell2);
        tableTitle.addCell(cell3);
        tableTitle.addCell(cell4);
        tableTitle.addCell(cell5);
        tableTitle.addCell(cell6);

        tableTitle.addCell(cellSalarioBase);
            tableTitle.addCell(c1sb);
            tableTitle.addCell(c2sb);
            tableTitle.addCell(c3sb);
            tableTitle.addCell(c4sb);
        tableTitle.addCell(cellProrrata);
            tableTitle.addCell(c1p);
            tableTitle.addCell(c2p);
            tableTitle.addCell(c3p);
            tableTitle.addCell(c4p);
        tableTitle.addCell(cellComplemento);
            tableTitle.addCell(c1com);
            tableTitle.addCell(c2com);
            tableTitle.addCell(c3com);
            tableTitle.addCell(c4com);
        tableTitle.addCell(cellAntiguedad);
            tableTitle.addCell(c1ant);
            tableTitle.addCell(c2ant);
            tableTitle.addCell(c3ant);
            tableTitle.addCell(c4ant);
        tableTitle.addCell(cellContingencias);
            tableTitle.addCell(c1cont);
            tableTitle.addCell(c2cont);
            tableTitle.addCell(c3cont);
            tableTitle.addCell(c4cont);
        tableTitle.addCell(cellDesempleo);
            tableTitle.addCell(c1des);
            tableTitle.addCell(c2des);
            tableTitle.addCell(c3des);
            tableTitle.addCell(c4des);
        tableTitle.addCell(cellCuotaForm);
            tableTitle.addCell(c1cuot);
            tableTitle.addCell(c2cuot);
            tableTitle.addCell(c3cuot);
            tableTitle.addCell(c4cuot);
        tableTitle.addCell(cellIRPF);
            tableTitle.addCell(c1irpf);
            tableTitle.addCell(c2irpf);
            tableTitle.addCell(c3irpf);
            tableTitle.addCell(c4irpf);
        tableTitle.addCell(deducciones);
            tableTitle.addCell(c1deduc);
            tableTitle.addCell(c4deduc);
        tableTitle.addCell(Devengos);
            tableTitle.addCell(c1dev);
            tableTitle.addCell(c3dev);
            tableTitle.addCell(c4dev);
        tableTitle.addCell(liquidoAPecibir);
            tableTitle.addCell(c1iliqu);
            tableTitle.addCell(c2iliqu);
            tableTitle.addCell(c3iliqu);
            tableTitle.addCell(c4iliqu);


        return tableTitle;
    }
    private PdfPTable createTable4()throws DocumentException{

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorder(0);// = 0;

        double importeSegSocEmpresario = round2Decimals(Double.parseDouble(contingenciasGenerales));
        double importeDesempleoEmpresario = round2Decimals(Double.parseDouble(desempleo_emp));
        double importeFormacionEmpresario = round2Decimals(Double.parseDouble(formacion_Emp));
        double importeAccidentesEmpresario =  round2Decimals(Double.parseDouble(accidentes));
        double importeFogasaEmpresario = round2Decimals(Double.parseDouble(fogasa));
        double importeTotalEmpresario = round2Decimals(Double.parseDouble(costeTotalTrabajador));
        double baseEmpresario = round2Decimals(Double.parseDouble(totalEmpresario));

        PdfPCell calculoEmpresario = new PdfPCell(new Phrase("Calculo empresario: BASE"));
            PdfPCell ceData = new PdfPCell(new Phrase((!isExtra ? brutoMes : "0")));

        PdfPCell contingencias = new PdfPCell(new Phrase("Contingencias comunes " + cuotaEmpresa.get(3).getValor() + "%"));
            PdfPCell conData = new PdfPCell(new Phrase("\t\t\t\t\t\t\t\t\t\t\t" + String.valueOf( round2Decimals(Double.parseDouble(contingenciasGenerales)))));

        PdfPCell desempleo = new PdfPCell(new Phrase("Desempleo " + cuotaEmpresa.get(5).getValor() + "% "));
            PdfPCell desData = new PdfPCell(new Phrase("\t\t\t\t\t\t\t\t\t\t\t" + String.valueOf( round2Decimals(Double.parseDouble(desempleo_emp)))));

        PdfPCell formacion = new PdfPCell(new Phrase("Formacion " + cuotaEmpresa.get(6).getValor()  + "%"));
            PdfPCell formData = new PdfPCell(new Phrase("\t\t\t\t\t\t\t\t\t\t\t" + String.valueOf( round2Decimals(Double.parseDouble(formacion_Emp)))));

        PdfPCell accidentes_cell = new PdfPCell(new Phrase("Accidentes de trabajo " +  cuotaEmpresa.get(7).getValor()  +"% "));
            PdfPCell accidentesData = new PdfPCell(new Phrase("\t\t\t\t\t\t\t\t\t\t\t" + String.valueOf( round2Decimals(Double.parseDouble(accidentes)))));

        PdfPCell fogasa_cell = new PdfPCell(new Phrase("FOGASA " + cuotaEmpresa.get(4).getValor() +"%"));
            PdfPCell fogData = new PdfPCell(new Phrase(String.valueOf("\t\t\t\t\t\t\t\t\t\t\t" +  round2Decimals(Double.parseDouble(fogasa)))));

        PdfPCell total = new PdfPCell(new Phrase("TOTAL empresario "));
            PdfPCell totData = new PdfPCell(new Phrase(String.valueOf( round2Decimals(Double.parseDouble(totalEmpresario)))));

        PdfPCell costeTotal = new PdfPCell(new Phrase("Coste total trabajador"));
            PdfPCell costotData = new PdfPCell(new Phrase(String.valueOf(round2Decimals(Double.parseDouble(costeTotalTrabajador)))));

            nomina.setImporteSegSocialEmpresario(importeSegSocEmpresario);
            nomina.setImporteDesempleoEmpresario(importeDesempleoEmpresario);
            nomina.setImporteFormacionEmpresario(importeFormacionEmpresario);
            nomina.setImporteAccidentesTrabajoEmpresario(importeAccidentesEmpresario);
            nomina.setImporteFogasaEmpresario(importeFogasaEmpresario);
            nomina.setCosteTotalEmpresario(importeTotalEmpresario);
            nomina.setBaseEmpresario(baseEmpresario);

            nomina.setSegSocialEmpresario(cuotaEmpresa.get(3).getValor());
            nomina.setDesempleoEmpresario(cuotaEmpresa.get(5).getValor());
            nomina.setFormacionEmpresario(cuotaEmpresa.get(6).getValor());
            nomina.setAccidentesTrabajoEmpresario(cuotaEmpresa.get(7).getValor());
            nomina.setFogasaEmpresario(cuotaEmpresa.get(4).getValor());

        table.setSpacingBefore(10f);
        table.setSpacingAfter(20f);

        table.addCell(calculoEmpresario);
        table.addCell(ceData);
        table.addCell(contingencias);
        table.addCell(conData);
        table.addCell(desempleo);
        table.addCell(desData);
        table.addCell(formacion);
        table.addCell(formData);
        table.addCell(accidentes_cell);
        table.addCell(accidentesData);
        table.addCell(fogasa_cell);
        table.addCell(fogData);
        table.addCell(total);
        table.addCell(totData);
        table.addCell(costeTotal);
        table.addCell(costotData);

        table.setHorizontalAlignment(Element.ALIGN_CENTER);

        return table;

    }

    public Nomina getNomina() {
        return nomina;
    }
}
