package org.application;

import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;

import org.data.Worker;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLWriter {

    public XMLWriter(ArrayList<Worker> repeatedWorkers) { //FIXME: Pasar al constructor un ArrayList de trabajadores y extraer de ahi los datos.
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            //root
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Trabajadores");
            doc.appendChild(rootElement);


            //this is where the loop should start.
            for (int i = 0; i < repeatedWorkers.size(); i++) {
                // Create attributes.
                Element trabajadores = doc.createElement("Trabajador");
                rootElement.appendChild(trabajadores);

                Attr attr = doc.createAttribute("id");
                attr.setValue(repeatedWorkers.get(i).getId());
                trabajadores.setAttributeNode(attr);

                Element nombre = doc.createElement("nombre");
                nombre.appendChild(doc.createTextNode(repeatedWorkers.get(i).getNombre()));
                trabajadores.appendChild(nombre);


                Element primerApellido = doc.createElement("PrimerApellido");
                primerApellido.appendChild(doc.createTextNode(repeatedWorkers.get(i).getPrimerApellido()));
                trabajadores.appendChild(primerApellido);

                Element segundoApellido = doc.createElement("SegundoApellido");
                segundoApellido.appendChild(doc.createTextNode(repeatedWorkers.get(i).getSegundoApellido()));
                trabajadores.appendChild(segundoApellido);

                Element categoria = doc.createElement("Categoria");
                categoria.appendChild(doc.createTextNode(repeatedWorkers.get(i).getCategoriaString()));
                trabajadores.appendChild(categoria);

                Element empresa = doc.createElement("Empresa");
                empresa.appendChild(doc.createTextNode(repeatedWorkers.get(i).getEmpresa()));
                trabajadores.appendChild(empresa);
            }
            //END OF LOOP

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "5");

            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("resources/Errores.xml"));

            transformer.transform(source,result);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

}
