package org.data;

public class Empresa {

    String nombreEmpresa;
    String cifEmpresa;

    public Empresa(String nombreEmpresa, String cifEmpresa){

        this.nombreEmpresa = nombreEmpresa;
        this.cifEmpresa = cifEmpresa;
    }

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof Empresa){
            Empresa ptr = (Empresa) v;
            retVal = ptr.getCifEmpresa().equals(this.cifEmpresa);
        }
        return retVal;
    }


    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public String getCifEmpresa() {
        return cifEmpresa;
    }
}
