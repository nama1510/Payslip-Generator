package org.data;

public class Worker {

    private String id;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private Category categoria;
    private String categoriaString;
    private String empresa;
    private String email;
    private String cifEmpresa;
    private String fechaAltaEmpresa;
    private String fechaAltaLaboral;
    private String fechaBajaLaboral;
    private String horasExtraForzadas;
    private String horasExtraVoluntarias;
    private boolean prorrataExtra;
    private String ccc;
    private String paisOrigenCuenta;
    private String iban;



    public Worker(String _id, String _nombre, String _primerApellido, String _segundoApellido, String _categoriaString, String _empresa){
        this.id = _id;
        this.nombre = _nombre;
        this.primerApellido = _primerApellido;
        this.segundoApellido = _segundoApellido;
        this.categoriaString = _categoriaString;
        this.empresa = _empresa;
    }

    public Worker(String id, String nombre, String primerApellido, String segundoApellido, Category categoria, String empresa, String email, String cifEmpresa, String fechaAltaEmpresa,
            String fechaAltaLaboral, String fechaBajaLaboral, String horasExtraForzadas, String horasExtraVoluntarias, String prorrataExtra, String ccc, String paisOrigenCuenta, String iban){
        this.id = id;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.categoria = categoria;
        this.empresa = empresa;
        this.email = email;
        this.cifEmpresa = cifEmpresa;
        if(fechaAltaEmpresa != null)
            this.fechaAltaEmpresa = fechaAltaEmpresa;
        else
            this.fechaAltaEmpresa = "";

        if(fechaAltaLaboral != null)
            this.fechaAltaLaboral = fechaAltaLaboral;
        else
            this.fechaAltaLaboral = "";

        if(fechaBajaLaboral != null)
            this.fechaBajaLaboral = fechaBajaLaboral;
        else
            this.fechaBajaLaboral = "";

        this.horasExtraForzadas = horasExtraForzadas;
        this.horasExtraVoluntarias = horasExtraVoluntarias;
        if(prorrataExtra.equals("SI"))
            this.prorrataExtra = true;
        else
            this.prorrataExtra = false;

        this.ccc = ccc;
        this.paisOrigenCuenta = paisOrigenCuenta;
        this.iban = iban;


    }
    //@Override
    public boolean checkEqual(Worker v) {
        boolean retVal = false;

        if (v instanceof Worker){
            Worker ptr = (Worker) v;

            retVal = ptr.getId() == this.id;
            if(!retVal)
                retVal = ptr.getNombre() == this.nombre && ptr.getPrimerApellido() == ptr.primerApellido;
        }
        return retVal;
    }
    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public Category getCategoria() {
        return categoria;
    }

    public String getEmpresa() {
        return empresa;
    }

    public String getEmail() {
        return email;
    }

    public String getCifEmpresa() {
        return cifEmpresa;
    }

    public String getFechaAltaEmpresa() {
        return fechaAltaEmpresa;
    }

    public String getFechaAltaLaboral() {
        return fechaAltaLaboral;
    }

    public String getFechaBajaLaboral() {
        return fechaBajaLaboral;
    }

    public String getHorasExtraForzadas() {
        return horasExtraForzadas;
    }

    public String getHorasExtraVoluntarias() {
        return horasExtraVoluntarias;
    }

    public boolean isProrrataExtra() {
        return prorrataExtra;
    }

    public String getCcc() {
        return ccc;
    }

    public String getPaisOrigenCuenta() {
        return paisOrigenCuenta;
    }

    public String getIban() {
        return iban;
    }

    public String getCategoriaString() {
        return categoriaString;
    }
}
