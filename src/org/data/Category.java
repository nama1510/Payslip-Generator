package org.data;

public class Category {
    private String name;
    private double salarioBase;
    private double complementos;
    private double codigoCotizacion;

    public Category(String name, double salarioBase, double complementos, double codigoCotizacion){
        this.name = name;
        this.salarioBase = salarioBase;
        this.complementos = complementos;
        this.codigoCotizacion = codigoCotizacion;

    }

    //@Override
    public boolean checkEqual(Category v) {
        boolean retVal = false;

        if (v instanceof Category){
            Category ptr = (Category) v;

            retVal = ptr.getName().equals(this.name);

        }
        return retVal;
    }
    public String getName() {
        return name;
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public double getComplementos() {
        return complementos;
    }

    public double getCodigoCotizacion() {
        return codigoCotizacion;
    }
}
