package org.data;

public class Nomina {

    private Worker trabajador;
    private double numTrienios;
    private double importeTrienios;
    private double importeSalarioMes;
    private double importeComplementoMes;
    private double valorProrrateo;
    private double brutoAnual;
    private double brutoNomina;
    private double irpf;
    private double importeIrpf;
    private double baseEmpresario;
    private double segSocialEmpresario;
    private double importeSegSocialEmpresario;
    private double desempleoEmpresario;
    private double importeDesempleoEmpresario;
    private double formacionEmpresario;
    private double importeFormacionEmpresario;
    private double accidentesTrabajoEmpresario;
    private double importeAccidentesTrabajoEmpresario;
    private double fogasaEmpresario;
    private double importeFogasaEmpresario;
    private double segSocialTrabajador;
    private double importeSegSocialTrabajador;
    private double desempleoTrabajador;
    private double importeDesempleoTrabajador;
    private double formacionTrabajador;
    private double importeFormacionTrabajador;
    private double costeTotalEmpresario;
    private double anio;
    private double mes;

    private double liquidoNomina;
    public Nomina(){

    }

    public Nomina(Worker trabajador, double numTrienios, double importeTrienios, double importeSalarioMes, double importeComplementoMes, double valorProrrateo, double brutoAnual, double irpf, double importeIrpf, double baseEmpresario, double segSocialEmpresario, double importeSegSocialEmpresario, double importeDesempleoEmpresario, double desempleoEmpresario, double formacionEmpresario, double importeFormacionEmpresario, double accidentesTrabajoEmpresario, double importeAccidentesTrabajoEmpresario, double fogasaEmpresario, double importeFogasaEmpresario, double segSocialTrabajador, double importeSegSocialTrabajador, double desempleoTrabajador, double importeDesempleoTrabajador, double formacionTrabajador, double importeFormacionTrabajador, double costeTotalEmpresario) {
        this.trabajador = trabajador;
        this.numTrienios = numTrienios;
        this.importeTrienios = importeTrienios;
        this.importeSalarioMes = importeSalarioMes;
        this.importeComplementoMes = importeComplementoMes;
        this.valorProrrateo = valorProrrateo;
        this.brutoAnual = brutoAnual;
        this.irpf = irpf;
        this.importeIrpf = importeIrpf;
        this.baseEmpresario = baseEmpresario;
        this.segSocialEmpresario = segSocialEmpresario;
        this.importeSegSocialEmpresario = importeSegSocialEmpresario;
        this.importeDesempleoEmpresario = importeDesempleoEmpresario;
        this.desempleoEmpresario = desempleoEmpresario;
        this.formacionEmpresario = formacionEmpresario;
        this.importeFormacionEmpresario = importeFormacionEmpresario;
        this.accidentesTrabajoEmpresario = accidentesTrabajoEmpresario;
        this.importeAccidentesTrabajoEmpresario = importeAccidentesTrabajoEmpresario;
        this.fogasaEmpresario = fogasaEmpresario;
        this.importeFogasaEmpresario = importeFogasaEmpresario;
        this.segSocialTrabajador = segSocialTrabajador;
        this.importeSegSocialTrabajador = importeSegSocialTrabajador;
        this.desempleoTrabajador = desempleoTrabajador;
        this.importeDesempleoTrabajador = importeDesempleoTrabajador;
        this.formacionTrabajador = formacionTrabajador;
        this.importeFormacionTrabajador = importeFormacionTrabajador;
        this.costeTotalEmpresario = costeTotalEmpresario;
    }

    public Worker getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Worker trabajador) {
        this.trabajador = trabajador;
    }

    public double getNumTrienios() {
        return numTrienios;
    }

    public void setNumTrienios(double numTrienios) {
        this.numTrienios = numTrienios;
    }

    public double getImporteTrienios() {
        return importeTrienios;
    }

    public void setImporteTrienios(double importeTrienios) {
        this.importeTrienios = importeTrienios;
    }

    public double getImporteSalarioMes() {
        return importeSalarioMes;
    }

    public void setImporteSalarioMes(double importeSalarioMes) {
        this.importeSalarioMes = importeSalarioMes;
    }

    public double getImporteComplementoMes() {
        return importeComplementoMes;
    }

    public void setImporteComplementoMes(double importeComplementoMes) {
        this.importeComplementoMes = importeComplementoMes;
    }

    public double getValorProrrateo() {
        return valorProrrateo;
    }

    public void setValorProrrateo(double valorProrrateo) {
        this.valorProrrateo = valorProrrateo;
    }

    public double getBrutoAnual() {
        return brutoAnual;
    }

    public void setBrutoAnual(double brutoAnual) {
        this.brutoAnual = brutoAnual;
    }

    public double getBrutoNomina() {
        return brutoNomina;
    }

    public void setBrutoNomina(double brutoNomina) {
        this.brutoNomina = brutoNomina;
    }

    public double getIrpf() {
        return irpf;
    }

    public void setIrpf(double irpf) {
        this.irpf = irpf;
    }

    public double getImporteIrpf() {
        return importeIrpf;
    }

    public void setImporteIrpf(double importeIrpf) {
        this.importeIrpf = importeIrpf;
    }

    public double getBaseEmpresario() {
        return baseEmpresario;
    }

    public void setBaseEmpresario(double baseEmpresario) {
        this.baseEmpresario = baseEmpresario;
    }

    public double getSegSocialEmpresario() {
        return segSocialEmpresario;
    }

    public void setSegSocialEmpresario(double segSocialEmpresario) {
        this.segSocialEmpresario = segSocialEmpresario;
    }

    public double getImporteSegSocialEmpresario() {
        return importeSegSocialEmpresario;
    }

    public void setImporteSegSocialEmpresario(double importeSegSocialEmpresario) {
        this.importeSegSocialEmpresario = importeSegSocialEmpresario;
    }

    public double getImporteDesempleoEmpresario() {
        return importeDesempleoEmpresario;
    }

    public void setImporteDesempleoEmpresario(double importeDesempleoEmpresario) {
        this.importeDesempleoEmpresario = importeDesempleoEmpresario;
    }

    public double getDesempleoEmpresario() {
        return desempleoEmpresario;
    }

    public void setDesempleoEmpresario(double desempleoEmpresario) {
        this.desempleoEmpresario = desempleoEmpresario;
    }

    public double getFormacionEmpresario() {
        return formacionEmpresario;
    }

    public void setFormacionEmpresario(double formacionEmpresario) {
        this.formacionEmpresario = formacionEmpresario;
    }

    public double getImporteFormacionEmpresario() {
        return importeFormacionEmpresario;
    }

    public void setImporteFormacionEmpresario(double importeFormacionEmpresario) {
        this.importeFormacionEmpresario = importeFormacionEmpresario;
    }

    public double getAccidentesTrabajoEmpresario() {
        return accidentesTrabajoEmpresario;
    }

    public void setAccidentesTrabajoEmpresario(double accidentesTrabajoEmpresario) {
        this.accidentesTrabajoEmpresario = accidentesTrabajoEmpresario;
    }

    public double getImporteAccidentesTrabajoEmpresario() {
        return importeAccidentesTrabajoEmpresario;
    }

    public void setImporteAccidentesTrabajoEmpresario(double importeAccidentesTrabajoEmpresario) {
        this.importeAccidentesTrabajoEmpresario = importeAccidentesTrabajoEmpresario;
    }

    public double getFogasaEmpresario() {
        return fogasaEmpresario;
    }

    public void setFogasaEmpresario(double fogasaEmpresario) {
        this.fogasaEmpresario = fogasaEmpresario;
    }

    public double getImporteFogasaEmpresario() {
        return importeFogasaEmpresario;
    }

    public void setImporteFogasaEmpresario(double importeFogasaEmpresario) {
        this.importeFogasaEmpresario = importeFogasaEmpresario;
    }

    public double getSegSocialTrabajador() {
        return segSocialTrabajador;
    }

    public void setSegSocialTrabajador(double segSocialTrabajador) {
        this.segSocialTrabajador = segSocialTrabajador;
    }

    public double getImporteSegSocialTrabajador() {
        return importeSegSocialTrabajador;
    }

    public void setImporteSegSocialTrabajador(double importeSegSocialTrabajador) {
        this.importeSegSocialTrabajador = importeSegSocialTrabajador;
    }

    public double getDesempleoTrabajador() {
        return desempleoTrabajador;
    }

    public void setDesempleoTrabajador(double desempleoTrabajador) {
        this.desempleoTrabajador = desempleoTrabajador;
    }

    public double getImporteDesempleoTrabajador() {
        return importeDesempleoTrabajador;
    }

    public void setImporteDesempleoTrabajador(double importeDesempleoTrabajador) {
        this.importeDesempleoTrabajador = importeDesempleoTrabajador;
    }

    public double getFormacionTrabajador() {
        return formacionTrabajador;
    }

    public void setFormacionTrabajador(double formacionTrabajador) {
        this.formacionTrabajador = formacionTrabajador;
    }

    public double getImporteFormacionTrabajador() {
        return importeFormacionTrabajador;
    }

    public void setImporteFormacionTrabajador(double importeFormacionTrabajador) {
        this.importeFormacionTrabajador = importeFormacionTrabajador;
    }

    public double getCosteTotalEmpresario() {
        return costeTotalEmpresario;
    }

    public void setCosteTotalEmpresario(double costeTotalEmpresario) {
        this.costeTotalEmpresario = costeTotalEmpresario;
    }

    public double getAnio() {
        return anio;
    }

    public void setAnio(double anio) {
        this.anio = anio;
    }

    public double getMes() {
        return mes;
    }

    public void setMes(double mes) {
        this.mes = mes;
    }

    public double getLiquidoNomina() {
        return liquidoNomina;
    }

    public void setLiquidoNomina(double liquidoNomina) {
        this.liquidoNomina = liquidoNomina;
    }
}
