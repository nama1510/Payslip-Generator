package org.data;

public class CuotaEmpresa {

    private String nombreCuota;
    private double valor;

    public CuotaEmpresa(String nombreCuota, double valor){

        this.nombreCuota = nombreCuota;
        this.valor = valor;
    }


    public String getNombreCuota() {
        return nombreCuota;
    }

    public double getValor() {
        return valor;
    }
}
