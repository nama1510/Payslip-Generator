package org.data;

public class BrutoRetenciones {
    private double bruto;
    private double retenciones;

    public BrutoRetenciones(double bruto, double retenciones){
        this.bruto = bruto;
        this.retenciones = retenciones;
    }

    public double getBruto() {
        return bruto;
    }

    public double getRetenciones() {
        return retenciones;
    }
}
