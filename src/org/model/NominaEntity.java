package org.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "nomina", schema = "nominas", catalog = "")
public class NominaEntity {
    private int idNomina;
    private int mes;
    private int anio;
    private int numeroTrienios;
    private double importeTrienios;
    private double importeSalarioMes;
    private double importeComplementoMes;
    private double valorProrrateo;
    private double brutoAnual;
    private double irpf;
    private double importeIrpf;
    private double baseEmpresario;
    private double seguridadSocialEmpresario;
    private double importeSeguridadSocialEmpresario;
    private double desempleoEmpresario;
    private double importeDesempleoEmpresario;
    private double formacionEmpresario;
    private double importeFormacionEmpresario;
    private double accidentesTrabajoEmpresario;
    private double importeAccidentesTrabajoEmpresario;
    private double fogasaEmpresario;
    private double importeFogasaeMpresario;
    private double seguridadSocialTrabajador;
    private double importeSeguridadSocialTrabajador;
    private double desempleoTrabajador;
    private double importeDesempleoTrabajador;
    private double formacionTrabajador;
    private double importeFormacionTrabajador;
    private double brutoNomina;
    private double liquidoNomina;
    private double costeTotalEmpresario;


    @JoinColumn(name = "IdTrabajador", unique = true)
    @OneToOne(cascade = CascadeType.MERGE)
    private TrabajadorbbddEntity trabajador;
    public TrabajadorbbddEntity getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(TrabajadorbbddEntity trabajador) {
        this.trabajador = trabajador;
    }


    @Id
    @Column(name = "IdNomina", nullable = false)
    public int getIdNomina() {
        return idNomina;
    }

    public void setIdNomina(int idNomina) {
        this.idNomina = idNomina;
    }

    @Basic
    @Column(name = "Mes", nullable = false)
    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    @Basic
    @Column(name = "Anio", nullable = false)
    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    @Basic
    @Column(name = "NumeroTrienios", nullable = false)
    public int getNumeroTrienios() {
        return numeroTrienios;
    }

    public void setNumeroTrienios(int numeroTrienios) {
        this.numeroTrienios = numeroTrienios;
    }

    @Basic
    @Column(name = "ImporteTrienios", nullable = false, precision = 0)
    public double getImporteTrienios() {
        return importeTrienios;
    }

    public void setImporteTrienios(double importeTrienios) {
        this.importeTrienios = importeTrienios;
    }

    @Basic
    @Column(name = "importeSalarioMes", nullable = false, precision = 0)
    public double getImporteSalarioMes() {
        return importeSalarioMes;
    }

    public void setImporteSalarioMes(double importeSalarioMes) {
        this.importeSalarioMes = importeSalarioMes;
    }

    @Basic
    @Column(name = "importeComplementoMes", nullable = false, precision = 0)
    public double getImporteComplementoMes() {
        return importeComplementoMes;
    }

    public void setImporteComplementoMes(double importeComplementoMes) {
        this.importeComplementoMes = importeComplementoMes;
    }

    @Basic
    @Column(name = "ValorProrrateo", nullable = false, precision = 0)
    public double getValorProrrateo() {
        return valorProrrateo;
    }

    public void setValorProrrateo(double valorProrrateo) {
        this.valorProrrateo = valorProrrateo;
    }

    @Basic
    @Column(name = "brutoAnual", nullable = false, precision = 0)
    public double getBrutoAnual() {
        return brutoAnual;
    }

    public void setBrutoAnual(double brutoAnual) {
        this.brutoAnual = brutoAnual;
    }

    @Basic
    @Column(name = "IRPF", nullable = false, precision = 0)
    public double getIrpf() {
        return irpf;
    }

    public void setIrpf(double irpf) {
        this.irpf = irpf;
    }

    @Basic
    @Column(name = "ImporteIRPF", nullable = false, precision = 0)
    public double getImporteIrpf() {
        return importeIrpf;
    }

    public void setImporteIrpf(double importeIrpf) {
        this.importeIrpf = importeIrpf;
    }

    @Basic
    @Column(name = "BaseEmpresario", nullable = false, precision = 0)
    public double getBaseEmpresario() {
        return baseEmpresario;
    }

    public void setBaseEmpresario(double baseEmpresario) {
        this.baseEmpresario = baseEmpresario;
    }

    @Basic
    @Column(name = "SeguridadSocialEmpresario", nullable = false, precision = 0)
    public double getSeguridadSocialEmpresario() {
        return seguridadSocialEmpresario;
    }

    public void setSeguridadSocialEmpresario(double seguridadSocialEmpresario) {
        this.seguridadSocialEmpresario = seguridadSocialEmpresario;
    }

    @Basic
    @Column(name = "ImporteSeguridadSocialEmpresario", nullable = false, precision = 0)
    public double getImporteSeguridadSocialEmpresario() {
        return importeSeguridadSocialEmpresario;
    }

    public void setImporteSeguridadSocialEmpresario(double importeSeguridadSocialEmpresario) {
        this.importeSeguridadSocialEmpresario = importeSeguridadSocialEmpresario;
    }

    @Basic
    @Column(name = "DesempleoEmpresario", nullable = false, precision = 0)
    public double getDesempleoEmpresario() {
        return desempleoEmpresario;
    }

    public void setDesempleoEmpresario(double desempleoEmpresario) {
        this.desempleoEmpresario = desempleoEmpresario;
    }

    @Basic
    @Column(name = "ImporteDesempleoEmpresario", nullable = false, precision = 0)
    public double getImporteDesempleoEmpresario() {
        return importeDesempleoEmpresario;
    }

    public void setImporteDesempleoEmpresario(double importeDesempleoEmpresario) {
        this.importeDesempleoEmpresario = importeDesempleoEmpresario;
    }

    @Basic
    @Column(name = "FormacionEmpresario", nullable = false, precision = 0)
    public double getFormacionEmpresario() {
        return formacionEmpresario;
    }

    public void setFormacionEmpresario(double formacionEmpresario) {
        this.formacionEmpresario = formacionEmpresario;
    }

    @Basic
    @Column(name = "ImporteFormacionEmpresario", nullable = false, precision = 0)
    public double getImporteFormacionEmpresario() {
        return importeFormacionEmpresario;
    }

    public void setImporteFormacionEmpresario(double importeFormacionEmpresario) {
        this.importeFormacionEmpresario = importeFormacionEmpresario;
    }

    @Basic
    @Column(name = "AccidentesTrabajoEmpresario", nullable = false, precision = 0)
    public double getAccidentesTrabajoEmpresario() {
        return accidentesTrabajoEmpresario;
    }

    public void setAccidentesTrabajoEmpresario(double accidentesTrabajoEmpresario) {
        this.accidentesTrabajoEmpresario = accidentesTrabajoEmpresario;
    }

    @Basic
    @Column(name = "ImporteAccidentesTrabajoEmpresario", nullable = false, precision = 0)
    public double getImporteAccidentesTrabajoEmpresario() {
        return importeAccidentesTrabajoEmpresario;
    }

    public void setImporteAccidentesTrabajoEmpresario(double importeAccidentesTrabajoEmpresario) {
        this.importeAccidentesTrabajoEmpresario = importeAccidentesTrabajoEmpresario;
    }

    @Basic
    @Column(name = "FOGASAEmpresario", nullable = false, precision = 0)
    public double getFogasaEmpresario() {
        return fogasaEmpresario;
    }

    public void setFogasaEmpresario(double fogasaEmpresario) {
        this.fogasaEmpresario = fogasaEmpresario;
    }

    @Basic
    @Column(name = "ImporteFOGASAEMpresario", nullable = false, precision = 0)
    public double getImporteFogasaeMpresario() {
        return importeFogasaeMpresario;
    }

    public void setImporteFogasaeMpresario(double importeFogasaeMpresario) {
        this.importeFogasaeMpresario = importeFogasaeMpresario;
    }

    @Basic
    @Column(name = "SeguridadSocialTrabajador", nullable = false, precision = 0)
    public double getSeguridadSocialTrabajador() {
        return seguridadSocialTrabajador;
    }

    public void setSeguridadSocialTrabajador(double seguridadSocialTrabajador) {
        this.seguridadSocialTrabajador = seguridadSocialTrabajador;
    }

    @Basic
    @Column(name = "ImporteSeguridadSocialTrabajador", nullable = false, precision = 0)
    public double getImporteSeguridadSocialTrabajador() {
        return importeSeguridadSocialTrabajador;
    }

    public void setImporteSeguridadSocialTrabajador(double importeSeguridadSocialTrabajador) {
        this.importeSeguridadSocialTrabajador = importeSeguridadSocialTrabajador;
    }

    @Basic
    @Column(name = "DesempleoTrabajador", nullable = false, precision = 0)
    public double getDesempleoTrabajador() {
        return desempleoTrabajador;
    }

    public void setDesempleoTrabajador(double desempleoTrabajador) {
        this.desempleoTrabajador = desempleoTrabajador;
    }

    @Basic
    @Column(name = "ImporteDesempleoTrabajador", nullable = false, precision = 0)
    public double getImporteDesempleoTrabajador() {
        return importeDesempleoTrabajador;
    }

    public void setImporteDesempleoTrabajador(double importeDesempleoTrabajador) {
        this.importeDesempleoTrabajador = importeDesempleoTrabajador;
    }

    @Basic
    @Column(name = "FormacionTrabajador", nullable = false, precision = 0)
    public double getFormacionTrabajador() {
        return formacionTrabajador;
    }

    public void setFormacionTrabajador(double formacionTrabajador) {
        this.formacionTrabajador = formacionTrabajador;
    }

    @Basic
    @Column(name = "ImporteFormacionTrabajador", nullable = false, precision = 0)
    public double getImporteFormacionTrabajador() {
        return importeFormacionTrabajador;
    }

    public void setImporteFormacionTrabajador(double importeFormacionTrabajador) {
        this.importeFormacionTrabajador = importeFormacionTrabajador;
    }

    @Basic
    @Column(name = "BrutoNomina", nullable = false, precision = 0)
    public double getBrutoNomina() {
        return brutoNomina;
    }

    public void setBrutoNomina(double brutoNomina) {
        this.brutoNomina = brutoNomina;
    }

    @Basic
    @Column(name = "LiquidoNomina", nullable = false, precision = 0)
    public double getLiquidoNomina() {
        return liquidoNomina;
    }

    public void setLiquidoNomina(double liquidoNomina) {
        this.liquidoNomina = liquidoNomina;
    }

    @Basic
    @Column(name = "CosteTotalEmpresario", nullable = false, precision = 0)
    public double getCosteTotalEmpresario() {
        return costeTotalEmpresario;
    }

    public void setCosteTotalEmpresario(double costeTotalEmpresario) {
        this.costeTotalEmpresario = costeTotalEmpresario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NominaEntity that = (NominaEntity) o;
        return idNomina == that.idNomina &&
                mes == that.mes &&
                anio == that.anio &&
                numeroTrienios == that.numeroTrienios &&
                Double.compare(that.importeTrienios, importeTrienios) == 0 &&
                Double.compare(that.importeSalarioMes, importeSalarioMes) == 0 &&
                Double.compare(that.importeComplementoMes, importeComplementoMes) == 0 &&
                Double.compare(that.valorProrrateo, valorProrrateo) == 0 &&
                Double.compare(that.brutoAnual, brutoAnual) == 0 &&
                Double.compare(that.irpf, irpf) == 0 &&
                Double.compare(that.importeIrpf, importeIrpf) == 0 &&
                Double.compare(that.baseEmpresario, baseEmpresario) == 0 &&
                Double.compare(that.seguridadSocialEmpresario, seguridadSocialEmpresario) == 0 &&
                Double.compare(that.importeSeguridadSocialEmpresario, importeSeguridadSocialEmpresario) == 0 &&
                Double.compare(that.desempleoEmpresario, desempleoEmpresario) == 0 &&
                Double.compare(that.importeDesempleoEmpresario, importeDesempleoEmpresario) == 0 &&
                Double.compare(that.formacionEmpresario, formacionEmpresario) == 0 &&
                Double.compare(that.importeFormacionEmpresario, importeFormacionEmpresario) == 0 &&
                accidentesTrabajoEmpresario == that.accidentesTrabajoEmpresario &&
                Double.compare(that.importeAccidentesTrabajoEmpresario, importeAccidentesTrabajoEmpresario) == 0 &&
                Double.compare(that.fogasaEmpresario, fogasaEmpresario) == 0 &&
                Double.compare(that.importeFogasaeMpresario, importeFogasaeMpresario) == 0 &&
                Double.compare(that.seguridadSocialTrabajador, seguridadSocialTrabajador) == 0 &&
                Double.compare(that.importeSeguridadSocialTrabajador, importeSeguridadSocialTrabajador) == 0 &&
                Double.compare(that.desempleoTrabajador, desempleoTrabajador) == 0 &&
                Double.compare(that.importeDesempleoTrabajador, importeDesempleoTrabajador) == 0 &&
                Double.compare(that.formacionTrabajador, formacionTrabajador) == 0 &&
                Double.compare(that.importeFormacionTrabajador, importeFormacionTrabajador) == 0 &&
                Double.compare(that.brutoNomina, brutoNomina) == 0 &&
                Double.compare(that.liquidoNomina, liquidoNomina) == 0 &&
                Double.compare(that.costeTotalEmpresario, costeTotalEmpresario) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idNomina, mes, anio, numeroTrienios, importeTrienios, importeSalarioMes, importeComplementoMes, valorProrrateo, brutoAnual, irpf, importeIrpf, baseEmpresario, seguridadSocialEmpresario, importeSeguridadSocialEmpresario, desempleoEmpresario, importeDesempleoEmpresario, formacionEmpresario, importeFormacionEmpresario, accidentesTrabajoEmpresario, importeAccidentesTrabajoEmpresario, fogasaEmpresario, importeFogasaeMpresario, seguridadSocialTrabajador, importeSeguridadSocialTrabajador, desempleoTrabajador, importeDesempleoTrabajador, formacionTrabajador, importeFormacionTrabajador, brutoNomina, liquidoNomina, costeTotalEmpresario);
    }
}
