package org.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "empresas", schema = "nominas", catalog = "")
public class EmpresasEntity {
    private int idEmpresa;
    private String nombre;
    private String cif;


    @Id
    @Column(name = "IdEmpresa", nullable = false)
    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Basic
    @Column(name = "Nombre", nullable = false, length = 100)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "CIF", nullable = false, length = 10)
    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmpresasEntity that = (EmpresasEntity) o;
        return idEmpresa == that.idEmpresa &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(cif, that.cif);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEmpresa, nombre, cif);
    }
}
