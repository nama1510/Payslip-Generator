package org.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "categorias", schema = "nominas", catalog = "")
public class CategoriasEntity {
    private int idCategoria;
    private String nombreCategoria;
    private double salarioBaseCategoria;
    private double complementoCategoria;


    @Id
    @Column(name = "IdCategoria", nullable = false)
    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Basic
    @Column(name = "NombreCategoria", nullable = false, length = 75)
    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    @Basic
    @Column(name = "SalarioBaseCategoria", nullable = false, precision = 0)
    public double getSalarioBaseCategoria() {
        return salarioBaseCategoria;
    }

    public void setSalarioBaseCategoria(double salarioBaseCategoria) {
        this.salarioBaseCategoria = salarioBaseCategoria;
    }

    @Basic
    @Column(name = "ComplementoCategoria", nullable = false, precision = 0)
    public double getComplementoCategoria() {
        return complementoCategoria;
    }

    public void setComplementoCategoria(double complementoCategoria) {
        this.complementoCategoria = complementoCategoria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoriasEntity that = (CategoriasEntity) o;
        return idCategoria == that.idCategoria &&
                Double.compare(that.salarioBaseCategoria, salarioBaseCategoria) == 0 &&
                Double.compare(that.complementoCategoria, complementoCategoria) == 0 &&
                Objects.equals(nombreCategoria, that.nombreCategoria);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idCategoria, nombreCategoria, salarioBaseCategoria, complementoCategoria);
    }
}
