package org.controller;


import java.util.ArrayList;
import java.util.List;


import org.data.Worker;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.model.*;


public class Operaciones {

    public void addEmpresa(EmpresasEntity empresasEntity){

        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session;
        session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<EmpresasEntity> registros = getEmpresasAll(session);
        for (EmpresasEntity registro : registros) {
            if(registro.getCif().equals(empresasEntity.getCif())){

                //Actualizar datos de la empresa.
                if(actualizarEmpresa(registro,empresasEntity)){
                    tx.commit();
                }

                session.close();
                System.out.println("Categoria " + empresasEntity.getNombre() + " " + empresasEntity.getCif() + " NO ha sido insertado en la base de datos");
                return;
            }
        }

        session.save(empresasEntity);

        tx.commit();
        session.close();

        System.out.println("Empresa " + empresasEntity.getNombre() + " insertado en la base de datos");

    }

    public void addWorker(TrabajadorbbddEntity trabajadorbbddEntity, String cifEmpresa, String categoria){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session;
        session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<CategoriasEntity> cEs = getCategoriasAll(session);
        List<EmpresasEntity> es = getEmpresasAll(session);
        for (CategoriasEntity c: cEs) {
            if(c.getNombreCategoria().equals(categoria)){
                trabajadorbbddEntity.setCategoria(c);
            }
        }

        for (EmpresasEntity e: es) {
            if(e.getCif().equals(cifEmpresa)) {
                trabajadorbbddEntity.setEmpresa(e);
            }
        }


        List<TrabajadorbbddEntity> registros = getTrabajadoresAll(session);
        for (TrabajadorbbddEntity registro : registros) {
            if(     registro.getNombre().equals(trabajadorbbddEntity.getNombre()) &&
                    registro.getApellido1().equals(trabajadorbbddEntity.getApellido1()) &&
                    registro.getApellido2().equals(trabajadorbbddEntity.getApellido2()) &&
                    registro.getNifnie().equals(trabajadorbbddEntity.getNifnie()) &&
                    registro.getFechaAlta().equals(trabajadorbbddEntity.getFechaAlta())){

                //Si ya existe el trabajador mirar si el resto de datos han sido actualizados
                if(actualizarTrabajador(registro, trabajadorbbddEntity)){
                    tx.commit();
                }
                session.close();
                System.out.println("Trabajador " + trabajadorbbddEntity.getNombre() + " NO ha sido insertado en la base de datos");
                return;
            }
        }

        session.save(trabajadorbbddEntity);

        tx.commit();
        session.close();

        System.out.println("Trabajador " + trabajadorbbddEntity.getNombre() + " insertado en la base de datos");

    }

    public void addCategoria(CategoriasEntity categoriasEntity){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session;
        session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();


        List<CategoriasEntity> registros = getCategoriasAll(session);
        for (CategoriasEntity registro : registros) {
            if(registro.getNombreCategoria().equals(categoriasEntity.getNombreCategoria())){

                if(actualizarCategoria(registro,categoriasEntity)){
                    tx.commit();
                }
                session.close();
                System.out.println("Categoria " + categoriasEntity.getNombreCategoria() + " NO ha sido insertado en la base de datos");
                return;
            }
        }

        session.save(categoriasEntity);
        tx.commit();

        session.close();

        System.out.println("Categoria " + categoriasEntity.getNombreCategoria() + " insertado en la base de datos");

    }

    public void addNomina(NominaEntity nominaEntity, Worker trabajador){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session;
        session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<TrabajadorbbddEntity> trabajadores = getTrabajadoresAll(session);
        for (TrabajadorbbddEntity t: trabajadores) {
            if(     t.getNombre().equals(trabajador.getNombre()) &&
                    t.getApellido1().equals(trabajador.getPrimerApellido()) &&
                    t.getApellido2().equals(trabajador.getSegundoApellido()) &&
                    t.getNifnie().equals(trabajador.getId())) {
                nominaEntity.setTrabajador(t);
            }
        }


        List<NominaEntity> registros = getNominasAll(session);
        for (NominaEntity registro : registros) {
            if(     registro.getMes() == nominaEntity.getMes() &&
                    registro.getAnio() == nominaEntity.getAnio() &&
                    registro.getBrutoNomina() == nominaEntity.getBrutoNomina() &&
                    registro.getLiquidoNomina() == nominaEntity.getLiquidoNomina() &&
                    registro.getTrabajador().equals(nominaEntity.getTrabajador())){

                if(actualizarNomina(registro,nominaEntity)){
                    tx.commit();
                }
                session.close();
                return;
            }
        }

        session.save(nominaEntity);
        tx.commit();

        session.close();

    }

    private boolean actualizarTrabajador(TrabajadorbbddEntity reg, TrabajadorbbddEntity entity){
        boolean[] modified = new boolean[4];
        if(!reg.getCodigoCuenta().equals(entity.getCodigoCuenta())) {
            reg.setCodigoCuenta(entity.getCodigoCuenta());
            modified[0] = true;
        }else{
            modified[0] = false;
        }
        if(!reg.getEmail().equals(entity.getEmail())) {
            reg.setEmail(entity.getEmail());
            modified[1] = true;
        }else{
            modified[1] = false;
        }
        if(!reg.getCategoria().equals(entity.getCategoria())) {
            reg.setCategoria(entity.getCategoria());
            modified[2] = true;
        }else{
            modified[2] = false;
        }
        if(!reg.getEmpresa().equals(entity.getEmpresa())) {
            reg.setEmpresa(entity.getEmpresa());
            modified[3] = true;
        }else{
            modified[3] = false;
        }
        if(modified[0] == true || modified[1] == true || modified[2] == true || modified[3] == true)
            return true;
        else
            return false;
    }

    private boolean actualizarCategoria(CategoriasEntity reg, CategoriasEntity entity){
        boolean[] modified = new boolean[2];
        if(reg.getSalarioBaseCategoria() != entity.getSalarioBaseCategoria()){
            reg.setSalarioBaseCategoria(entity.getSalarioBaseCategoria());
            modified[0] = true;
        }else{
            modified[0] = false;
        }
        if(reg.getComplementoCategoria() != entity.getComplementoCategoria()){
            reg.setComplementoCategoria(entity.getComplementoCategoria());
            modified[1] = true;
        }else{
            modified[1] = false;
        }

        if(modified[0] == true || modified[1] == true)
            return true;
        else
            return false;

    }

    private boolean actualizarEmpresa(EmpresasEntity reg, EmpresasEntity entity){
        if(reg.getNombre().equals(entity.getNombre())){
            reg.setNombre(entity.getNombre());
            return true;
        }else{
            return false;
        }

    }

    private boolean actualizarNomina(NominaEntity reg, NominaEntity entity){
        ArrayList<Boolean> modified = new ArrayList<Boolean>();
        if(reg.getNumeroTrienios() != entity.getNumeroTrienios()){
            reg.setNumeroTrienios(entity.getNumeroTrienios());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getImporteTrienios() != entity.getImporteTrienios()){
            reg.setImporteTrienios(entity.getImporteTrienios());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getImporteSalarioMes() != entity.getImporteSalarioMes()){
            reg.setImporteSalarioMes(entity.getImporteSalarioMes());
            modified.add(true);
        }else{
            modified.add(false);
        }
        if(reg.getImporteComplementoMes() != entity.getImporteComplementoMes()){
            reg.setImporteComplementoMes(entity.getImporteComplementoMes());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getValorProrrateo() != entity.getValorProrrateo()){
            reg.setValorProrrateo(entity.getValorProrrateo());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getBrutoAnual() != entity.getBrutoAnual()){
            reg.setBrutoAnual(entity.getBrutoAnual());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getIrpf() != entity.getIrpf()){
            reg.setIrpf(entity.getIrpf());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getImporteIrpf() != entity.getImporteIrpf()){
            reg.setImporteIrpf(entity.getImporteIrpf());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getBaseEmpresario() != entity.getBaseEmpresario()){
            reg.setBaseEmpresario(entity.getBaseEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getSeguridadSocialEmpresario() != entity.getSeguridadSocialEmpresario()){
            reg.setSeguridadSocialEmpresario(entity.getSeguridadSocialEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getImporteSeguridadSocialEmpresario() != entity.getImporteSeguridadSocialEmpresario()){
            reg.setImporteSeguridadSocialEmpresario(entity.getImporteSeguridadSocialEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }


        if(reg.getDesempleoEmpresario() != entity.getDesempleoEmpresario()){
            reg.setDesempleoEmpresario(entity.getDesempleoEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getImporteDesempleoEmpresario() != entity.getImporteDesempleoEmpresario()){
            reg.setImporteDesempleoEmpresario(entity.getImporteDesempleoEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getFormacionEmpresario() != entity.getFormacionEmpresario()){
            reg.setFormacionEmpresario(entity.getFormacionEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getImporteFormacionEmpresario() != entity.getImporteFormacionEmpresario()){
            reg.setImporteFormacionEmpresario(entity.getImporteFormacionEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getAccidentesTrabajoEmpresario() != entity.getAccidentesTrabajoEmpresario()){
            reg.setAccidentesTrabajoEmpresario(entity.getAccidentesTrabajoEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getImporteAccidentesTrabajoEmpresario() != entity.getImporteAccidentesTrabajoEmpresario()){
            reg.setImporteAccidentesTrabajoEmpresario(entity.getImporteAccidentesTrabajoEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getFogasaEmpresario() != entity.getFogasaEmpresario()){
            reg.setFogasaEmpresario(entity.getFogasaEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }
        if(reg.getImporteFogasaeMpresario() != entity.getImporteFogasaeMpresario()){
            reg.setImporteFogasaeMpresario(entity.getImporteFogasaeMpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getSeguridadSocialTrabajador() != entity.getSeguridadSocialTrabajador()){
            reg.setSeguridadSocialTrabajador(entity.getSeguridadSocialTrabajador());
            modified.add(true);
        }else{
            modified.add(false);
        }


        if(reg.getImporteSeguridadSocialTrabajador() != entity.getImporteSeguridadSocialTrabajador()){
            reg.setImporteSeguridadSocialTrabajador(entity.getImporteSeguridadSocialTrabajador());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getDesempleoTrabajador() != entity.getDesempleoTrabajador()){
            reg.setDesempleoTrabajador(entity.getDesempleoTrabajador());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getImporteDesempleoTrabajador() != entity.getImporteDesempleoTrabajador()){
            reg.setImporteDesempleoTrabajador(entity.getImporteDesempleoTrabajador());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getFormacionTrabajador() != entity.getFormacionTrabajador()){
            reg.setFormacionTrabajador(entity.getFormacionTrabajador());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getImporteFormacionTrabajador() != entity.getImporteFormacionTrabajador()){
            reg.setImporteFormacionTrabajador(entity.getImporteFormacionTrabajador());
            modified.add(true);
        }else{
            modified.add(false);
        }

        if(reg.getCosteTotalEmpresario() != entity.getCosteTotalEmpresario()){
            reg.setCosteTotalEmpresario(entity.getCosteTotalEmpresario());
            modified.add(true);
        }else{
            modified.add(false);
        }


        for (boolean m : modified) {
            if (m == true)
                return true;
        }
        return false;

    }

    @SuppressWarnings("unchecked")
    public List<CategoriasEntity> getCategoriasAll(Session session){
        return session.createCriteria(CategoriasEntity.class).list();
    }

    @SuppressWarnings("unchecked")
    public List<EmpresasEntity> getEmpresasAll(Session session){
        return session.createCriteria(EmpresasEntity.class).list();
    }

    @SuppressWarnings("unchecked")
    public List<TrabajadorbbddEntity> getTrabajadoresAll(Session session){
        return session.createCriteria(TrabajadorbbddEntity.class).list();
    }
    @SuppressWarnings("unchecked")
    public List<NominaEntity> getNominasAll(Session session){
        return session.createCriteria(NominaEntity.class).list();
    }



}
