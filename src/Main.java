import org.application.CalculadorNominas;
import org.application.ExcelReader;
import org.controller.Operaciones;
import org.data.Category;
import org.data.Empresa;
import org.data.Nomina;
import org.data.Worker;
import org.model.CategoriasEntity;
import org.model.EmpresasEntity;
import org.model.NominaEntity;
import org.model.TrabajadorbbddEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        ArrayList<Empresa> empresas = new ArrayList<Empresa>();


        try {
            ExcelReader er = new ExcelReader("SistemasInformacionII.xlsx");


            if(er.getCategoria_2CellIndex() == -1 || er.getSalarioBaseCellIndex() == -1 || er.getComplementosCellIndex() == -1 || er.getCodigoCotizacionCellIndex() == -1||
                    er.getBrutoAnualCellIndex() == -1 || er.getRetencionCellIndex() == -1){

                System.out.println("Fallo en la segunda hoja de el archivo xlsx.");
            }


            if (er.getNifNieCellIndex() != -1)
                er.validate(er.getSheet_1(), er.getNifNieCellIndex());
            else
                System.out.println("No existe ninguna columna \"NIFNIE\" en el archivo xlsx.");

            if (er.getCccCellIndex() == -1)
                System.out.println("No existe ninguna columna \"CodigoCuenta\" en el archivo xlsx.");
            else if (er.getIbanCellIndex()== -1)
                System.out.println("No existe ninguna columna \"IBAN\" en el archivo xlsx.");
            else if (er.getPaisOrigenCellIndex()== -1)
                System.out.println("No existe ninguna columna \"Pais Origen Cuenta Bancaria\" en el archivo xlsx.");
            else {
                er.validateCCC(er.getCccCellIndex());
                er.calculateIban(er.getIbanCellIndex(), er.getCccCellIndex(), er.getPaisOrigenCellIndex());
            }

            if (er.getEmailCellIndex()== -1)
                System.out.println("No existe ninguna columna \"Email\" en el archivo xlsx.");
            else if (er.getNameCellIndex() == -1)
                System.out.println("No existe ninguna columna \"Nombre\" en el archivo xlsx.");
            else if (er.getLastName1CellIndex() == -1)
                System.out.println("No existe ninguna \"Apellido1\" en el archivo xlsx.");
            else if (er.getLastName2CellIndex() == -1)
                System.out.println("No existe ninguna columna \"Apellido2\" en el archivo xlsx.");
            else if (er.getLastName2CellIndex() == -1)
                System.out.println("No existe ninguna \"Nombre empresa\" en el archivo xlsx.");
            else
                er.generateEmail(er.getEmailCellIndex(), er.getNameCellIndex(), er.getLastName1CellIndex(),
                        er.getLastName2CellIndex(), er.getCompanyCellIndex());

            er.AddWorkersToArrayList();
            er.closeWorkBook();


            //int mes = 03;
            //int anio = 2020;
            boolean isRunning = true;
            while(isRunning){
                System.out.print("Introduzca una fecha (mm/aaaa) o si desea salir introduzca 0: ");
                Scanner input = new Scanner(System.in);
                String fecha = input.nextLine();

                if(fecha.equals("0"))
                    isRunning = false;
                else{
                    int mes = 1;
                    int anio = 1;
                    boolean datosCorrectos;
                    try{
                        mes = Integer.parseInt(fecha.substring(0, 2));
                        anio = Integer.parseInt(fecha.substring(3, 7));
                        datosCorrectos = ComprobarErrores(mes,anio);
                    }
                    catch(NumberFormatException e){
                        datosCorrectos = false;
                    }
                    if(datosCorrectos) {
                        CalculadorNominas cn = new CalculadorNominas(er.getAllWorkers(), er.getValoresTrienio(), er.getCuotasEmpresa(), er.getBrutoRetenciones(), mes, anio);
                        empresas = CrearArrayListEmpresas(er.getAllWorkers());
                        //Aniadir a la base de datos.

                        // 1 - Aniadir Categorias
                        for (int i = 0; i < er.getCategorias().size(); i++){
                            addCategoryToDatabase(er.getCategorias().get(i));
                        }
                        // 2 - Aniadir Empresas
                        for (int i = 0; i < empresas.size(); i ++){
                            addEmpresaToDatabase(empresas.get(i));
                        }
                        // 3 - Aniadir Trabajadores
                        for (int i = 0; i < er.getAllWorkers().size(); i ++) {
                            addWorkerToDatabase(er.getAllWorkers().get(i));
                        }
                        // 4 - Aniadir Nominas
                        for (int i = 0; i < cn.getNominasGeneradas().size(); i ++) {
                            addNominaToDatabase(cn.getNominasGeneradas().get(i));
                        }
                    }else{
                        System.out.println("Los datos introducidos son incorrectos.");
                    }
                }
            }






        }catch(Exception e) {
            e.printStackTrace();
            System.out.println("Ha habido un error.");
        }

    }
    private static void addWorkerToDatabase(Worker w){

        Operaciones op = new Operaciones();

        if(!w.getId().equals("")) {
            TrabajadorbbddEntity te = new TrabajadorbbddEntity();
            te.setNombre(w.getNombre());
            te.setApellido1(w.getPrimerApellido());
            te.setApellido2(w.getSegundoApellido());
            te.setNifnie(w.getId());
            te.setEmail(w.getEmail());
            Date date = parseDate(w.getFechaAltaEmpresa());
            te.setFechaAlta(new java.sql.Date(date.getTime()));
            te.setCodigoCuenta(w.getCcc());
           // te.setIban("ES6621000418401234567891"); //FIXME: Poner el iban correcto una vez que este arreglado el metodo.
            te.setIban(w.getIban());

            op.addWorker(te,w.getCifEmpresa(),w.getCategoria().getName());
        }
    }

    private static void addCategoryToDatabase(Category cat){
        if(!cat.getName().equals("")) {
            Operaciones op = new Operaciones();

            CategoriasEntity ce = new CategoriasEntity();
            //ce.setIdCategoria(id);
            ce.setNombreCategoria(cat.getName());
            ce.setComplementoCategoria(cat.getComplementos());
            ce.setSalarioBaseCategoria(cat.getSalarioBase());

            op.addCategoria(ce);
        }

    }

    private static void addEmpresaToDatabase(Empresa em){
        if(!em.getNombreEmpresa().equals("") && !em.getCifEmpresa().equals("") ) {
            Operaciones op = new Operaciones();

            EmpresasEntity ee = new EmpresasEntity();
            ee.setNombre(em.getNombreEmpresa());
            ee.setCif(em.getCifEmpresa());

            op.addEmpresa(ee);
        }

    }


    private static void addNominaToDatabase(Nomina nomina){
        Operaciones op = new Operaciones();
        NominaEntity ne = new NominaEntity();

        ne.setMes((int) nomina.getMes());
        ne.setAnio((int) nomina.getAnio());
        ne.setNumeroTrienios((int)nomina.getNumTrienios());
        ne.setImporteTrienios(nomina.getImporteTrienios());
        ne.setImporteSalarioMes(nomina.getImporteSalarioMes());
        ne.setImporteComplementoMes(nomina.getImporteComplementoMes());
        ne.setValorProrrateo(nomina.getValorProrrateo());
        ne.setBrutoAnual(nomina.getBrutoAnual());
        ne.setIrpf(nomina.getIrpf());
        ne.setImporteIrpf(nomina.getImporteIrpf());

        ne.setBaseEmpresario(nomina.getBaseEmpresario());
        ne.setSeguridadSocialEmpresario(nomina.getSegSocialEmpresario());
        ne.setImporteSeguridadSocialEmpresario(nomina.getImporteSegSocialEmpresario());
        ne.setDesempleoEmpresario(nomina.getDesempleoEmpresario());
        ne.setImporteDesempleoEmpresario(nomina.getImporteDesempleoEmpresario());
        ne.setFormacionEmpresario(nomina.getFormacionEmpresario());
        ne.setImporteFormacionEmpresario(nomina.getImporteFormacionEmpresario());
        ne.setAccidentesTrabajoEmpresario(nomina.getAccidentesTrabajoEmpresario());
        ne.setImporteAccidentesTrabajoEmpresario(nomina.getImporteAccidentesTrabajoEmpresario());
        ne.setFogasaEmpresario(nomina.getFogasaEmpresario());
        ne.setImporteFogasaeMpresario(nomina.getImporteFogasaEmpresario());

        ne.setSeguridadSocialTrabajador(nomina.getSegSocialTrabajador());
        ne.setImporteSeguridadSocialTrabajador(nomina.getImporteSegSocialTrabajador());
        ne.setDesempleoTrabajador(nomina.getDesempleoTrabajador());
        ne.setImporteDesempleoTrabajador(nomina.getImporteDesempleoTrabajador());
        ne.setFormacionTrabajador(nomina.getFormacionTrabajador());
        ne.setImporteFormacionTrabajador(nomina.getImporteFormacionTrabajador());
        ne.setBrutoNomina(nomina.getBrutoNomina());
        ne.setLiquidoNomina(nomina.getLiquidoNomina());
        ne.setCosteTotalEmpresario(nomina.getCosteTotalEmpresario());


        op.addNomina(ne, nomina.getTrabajador());
    }


    public static ArrayList<Empresa> CrearArrayListEmpresas(ArrayList<Worker> trabajadores){

        ArrayList<Empresa> emps = new ArrayList<Empresa>();
        for (Worker trabajador : trabajadores) {
            Empresa emp = new Empresa(trabajador.getEmpresa(), trabajador.getCifEmpresa());
            if(!emps.contains(emp)){
                emps.add(emp);
            }
        }

        return emps;
    }
    public static Date parseDate(String date) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    private static boolean ComprobarErrores(int mes, int anio) {
        if(mes <= 0 || mes >12)return false;
        if(anio < 1980) return false;
        return true;
    }


}
